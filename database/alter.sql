/* Jitendra 31-05-18 */
ALTER TABLE `users` ADD `lastname` VARCHAR(255) NULL AFTER `name`, ADD `speciality` VARCHAR(255) NULL AFTER `lastname`, ADD `province` VARCHAR(255) NULL AFTER `speciality`;
ALTER TABLE `users` CHANGE `name` `firstname` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE `users` ADD `active` INT NOT NULL DEFAULT '0' COMMENT '0:inactive, 1:active' AFTER `password`;
ALTER TABLE `users` ADD `email_token` VARCHAR(255) NULL AFTER `remember_token`;


/* Ashok */

ALTER TABLE `enquiries` CHANGE `expert_id` `consultant_id` INT(11) NOT NULL;
ALTER TABLE `enquiries` CHANGE `consultant_id` `consultant_id` INT(11) NULL;
ALTER TABLE `enquiries` ADD `consultant_assigne_on` TIMESTAMP NULL AFTER `consultant_id`;
ALTER TABLE `users` ADD `profile_pic` VARCHAR(300) NULL AFTER `is_active`;

ALTER TABLE `users` ADD `username` VARCHAR(255) NULL AFTER `id`;
ALTER TABLE `users` CHANGE `phone` `phone` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `enquiries` CHANGE `consultant_id` `consultant_id` INT(11) NULL;

ALTER TABLE `users` ADD `profile_pic` VARCHAR(255) NULL AFTER `email`, ADD `cover_image` VARCHAR(255) NULL AFTER `profile_pic`;
ALTER TABLE `enquiries` ADD `doc` VARCHAR(255) NULL AFTER `email`;

CREATE TABLE `xavier_scholormd`.`highlight` ( `id` INT NOT NULL AUTO_INCREMENT ,  `user_id` INT NULL ,  `highlight` TEXT NULL ,  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,  `updated_at` TIMESTAMP NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;
