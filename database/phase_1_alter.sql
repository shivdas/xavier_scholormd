/*Added By Sameer*/
ALTER TABLE `facts` ADD `deleted_at` TIMESTAMP NULL AFTER `updated_at`;
ALTER TABLE `facts` ADD `is_active` TINYINT(4) NOT NULL DEFAULT '1' AFTER `fact_date`;

ALTER TABLE `places` ADD `deleted_at` TIMESTAMP NULL AFTER `updated_at`;

ALTER TABLE `places` CHANGE `category_type` `category_id` INT(11) NOT NULL;

ALTER TABLE `places` CHANGE `place_image` `place_image` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `sub_title` `sub_title` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `map` `map` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;

ALTER TABLE `categories` ADD `title` VARCHAR(100) NULL AFTER `cat_name`;

ALTER TABLE `categories` ADD `sub_title` VARCHAR(100) NULL AFTER `title`;

ALTER TABLE `categories` ADD `cat_image` VARCHAR(100) NULL AFTER `sub_title`;

ALTER TABLE `categories` ADD `map` TEXT NULL AFTER `cat_image`;

ALTER TABLE `categories` ADD `slug` VARCHAR(191) NOT NULL AFTER `cat_name`;


/*New changes*/
ALTER TABLE `facts` CHANGE `fact_date` `fact_date` DATE NULL;
ALTER TABLE `places` ADD `description` TEXT NOT NULL AFTER `map`;