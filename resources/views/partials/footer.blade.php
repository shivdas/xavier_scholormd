<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
     <strong>Copyright &copy; {{date('Y')}} All rights
    reserved.
  </footer>
@section('js')
    <script src="{{asset('js/sweetalert.min.js')}}"></script>
    <script type="text/javascript">

            setTimeout(function() {
                $('.x_title').fadeOut('fast');
            }, 4000);
    </script>