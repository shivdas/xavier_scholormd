<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('images/user.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="{{ (Request::is('admin/dashboard') ? 'active' : '') }}" >
          <a href="{{url('admin/dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

       
        <li class="treeview">
              <a href="#">
                  <i class="fa fa-user"></i>
                  <span>Manage Category</span>
                  <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{url('admin/category/create')}}"><i class="fa fa-circle-o"></i>Create Category</a></li>
                  <li><a href="{{url('admin/category')}}"><i class="fa fa-circle-o"></i>All Categories</a></li>
              </ul>
        </li>
         <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Manage Subcategory</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/subcat/create')}}"><i class="fa fa-circle-o"></i>Create Subcategory</a></li>
                    <li><a href="{{url('admin/subcat')}}"><i class="fa fa-circle-o"></i>All Subcategory</a></li>
                </ul>
        </li>

      <li class="treeview">
          <a href="#">
              <i class="fa fa-tasks"></i>
              <span>Manage Chapters</span>
              <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{url('admin/chapter/create')}}"><i class="fa fa-circle-o"></i>Create Chapters</a></li>
              <li><a href="{{url('admin/chapter')}}"><i class="fa fa-circle-o"></i>All Chapters</a></li>
          </ul>
      </li>

          <li class="treeview">
              <a href="#">
                  <i class="fa fa-mortar-board"></i>
                  <span>Manage Consultant</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{url('admin/consultant/create')}}"><i class="fa fa-circle-o"></i>Create Consultant</a></li>
                  <li><a href="{{url('admin/consultant')}}"><i class="fa fa-circle-o"></i>All Consultant</a></li>
              </ul>
          </li>

          <li class="treeview">
              <a href="#">
                  <i class="fa fa-suitcase"></i>
                  <span>Medical Post</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{url('admin/medical/create')}}"><i class="fa fa-circle-o"></i>Create Post</a></li>
                  <li><a href="{{url('admin/medical')}}"><i class="fa fa-circle-o"></i>All Post</a></li>
              </ul>
          </li>

          <li class="treeview">
              <a href="#">
                  <i class="fa fa-meh-o"></i>
                  <span>Manage Users</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{url('admin/user')}}"><i class="fa fa-circle-o"></i>All Users</a></li>
              </ul>
          </li>

          <li class="treeview">
              <a href="#">
                  <i class="fa fa-comment"></i>
                  <span>Manage Enquiry</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{url('admin/enquirie')}}"><i class="fa fa-circle-o"></i>All Enquiry</a></li>
              </ul>
          </li>
          <li class="treeview">
              <a href="#">
                  <i class="fa fa-creative-commons"></i>
                  <span>User Selections</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{url('admin/selection')}}"><i class="fa fa-circle-o"></i>All Selections</a></li>
              </ul>
          </li>

          {{--<li class="treeview">
              <a href="#">
                  <i class="fa fa-users"></i>
                  <span>Manage Facts</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{url('admin/facts')}}"><i class="fa fa-circle-o"></i>All Facts</a></li>
              </ul>
          </li>--}}

        <li class="{{ (Request::is('admin/cms*') ? 'active' : '') }} treeview">
                <a href="{{ url('/admin/cms') }}">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <span>Manage Cms</span>
                </a>
          </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>