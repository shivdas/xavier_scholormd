@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage All Categories</h3>
                    </div>
                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        <a href="{{route('category.create')}}" class="btn btn-info">Create Category</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="categoryTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                {{--<th>Sr No.</th>--}}
                                <th>Category Name</th>
                                <th>Sub Title</th>
                                <th>Created On</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($allCategory as $category)
                                <tr>
                                    {{--<td>{{$category->id}}</td>--}}
                                    <td>{{ucfirst($category->cat_name)}}</td>
                                    <td>{{ucfirst($category->sub_title)}}</td>
                                    <td>{{date("F jS, Y", strtotime($category->created_at))}}</td>
                                    <td>
                                        @if($category->is_active)
                                            <a data-cat_id="{{$category->id}}" data-cat_status="{{$category->is_active}}"    class="btn btn-primary cat_status">Active</a>
                                        @else
                                            <a data-cat_id="{{$category->id}}" data-cat_status="{{$category->is_active}}"   class="btn btn-danger cat_status">In-Active</a>
                                        @endif
                                        <a href="{{route('category.edit',['id'=>$category->id])}}" class="btn btn-warning" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                        <a type="button" data-cat_id="{{$category->id}}" class="btn btn-danger delete_cat" title="Delete"><i class="glyphicon glyphicon-edit"></i> Delete</a>

                                        {{--<a href="{{route('places.allPlaces',['id'=>$category->id])}}" class="btn btn-primary">View All</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')

    <script type="text/javascript">

       $('#categoryTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
           order: [ [0, 'desc'] ]
        });


       $('.delete_cat').on('click',function(e){
           e.preventDefault();
           var csrf_token  = "{{ csrf_token() }}";
           var id =$(this).attr('data-cat_id');
           var whichtr = $(this).closest("tr");

           swal({
               title: "Are you sure ?",
               text: "You want to delete this category ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
               .then((willDelete) => {
                   if (willDelete) {

                       $.ajax({
                           type: "GET",
                           url: "{{route('category.destroy')}}",
                           data: {id:id,_token:csrf_token},
                           success: function(result) {

                               if(result.type == 'success'){

                                   whichtr.remove();
                                   swal('Category successfully deleted','success');

                               }else{
                                   swal('Whoa!','Something Went Wrong.','error');
                               }
                           }
                       });

                   } else {
                       swal("Your category is safe!");
                   }
               });

       });


       $('.cat_status').on('click',function(e){
           e.preventDefault();
           var csrf_token  = "{{ csrf_token() }}";
           var id = $(this).attr('data-cat_id');
           var status = $(this).attr('data-cat_status');

           swal({
               title: "Are you sure ?",
               text: "Are you sure you want to change status ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
               .then((willDelete) => {
                   if (willDelete) {

                       $.ajax({
                           type: "GET",
                           url: "{{route('category.status')}}",
                           data: {id:id,status:status,_token:csrf_token},
                           success: function(result) {
                               if(result.type == 'success'){
                                   location.reload();
                               }
                           }
                       });

                   } else {
                       swal("Your status remian unchange !");
                   }
               });

       });

    </script>
@endsection