@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="addCategoryForm" action="{{route('category.store')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body"> 
                            <div class="form-group">
                                <label for="name">Category Name</label>
                                <input type="text" class="form-control" name="cat_name" placeholder="Enter Category Name.." value="{{old('cat_name')}}">
                                 @if ($errors->has('cat_name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('cat_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Sub Title</label>
                                <input type="text" class="form-control" name="sub_title" placeholder="Enter Sub Title.." value="{{old('sub_title')}}">
                                @if ($errors->has('sub_title'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('sub_title') }}</strong>
                                    </span>
                                @endif
                            </div>

                        <div class="box-footer">
                            <a href="{{url('admin/category')}}" class="btn btn-success">Cancel</a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')

<script type="text/javascript">

 $(function() {

     $('#addCategoryForm').validate({
         errorClass: 'text-danger',
         rules: {

             cat_name:"required"
         },
         messages: {
             cat_name: "Please enter category name"
         },
         submitHandler: function(form) {
             form.submit();
         }
     });
 });

</script>

@stop