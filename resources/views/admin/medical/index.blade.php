@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Medical Post</h3>
                    </div>
                    <div class="addMedical" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        <a href="{{route('medical.create')}}" class="btn btn-info">Create Post</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="medicalTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                {{--<th>Sr No.</th>--}}
                                <th>Post Name</th>
                                <th>Short Description</th>
                                <th>Detailed Description</th>
                                <th>Created On</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($allMedical as $medical)
                                <tr>
                                    {{--<td>{{$medical->id}}</td>--}}
                                    <td>{{ucfirst($medical->post_name)}}</td>
                                    <td title="{{ucfirst(strip_tags($medical->short_desc))}}">{{str_limit(ucfirst(strip_tags($medical->short_desc)),50)}}</td>
                                    <td title="{{ucfirst(strip_tags($medical->lagre_desc))}}">{{str_limit(ucfirst(strip_tags($medical->lagre_desc)),50)}}</td>
                                    <td>{{date("F jS, Y", strtotime($medical->created_at))}}</td>
                                    <td>
                                        @if($medical->is_active)
                                            <a data-medical_id="{{$medical->id}}" data-medical_status="{{$medical->is_active}}"    class="btn btn-primary medical_status">Active</a>
                                        @else
                                            <a data-medical_id="{{$medical->id}}" data-medical_status="{{$medical->is_active}}"   class="btn btn-danger medical_status">In-Active</a>
                                        @endif
                                        <a href="{{route('medical.edit',['id'=>$medical->id])}}" class="btn btn-warning" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                        <a type="button" data-medical_id="{{$medical->id}}" class="btn btn-danger delete_medical" title="Delete"><i class="glyphicon glyphicon-edit"></i> Delete</a>

                                        {{--<a href="{{route('places.allPlaces',['id'=>$medical->id])}}" class="btn btn-primary">View All</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')

    <script type="text/javascript">

       $('#medicalTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
           order: [ [0, 'desc'] ]
        });


       $('.delete_medical').on('click',function(e){
           e.preventDefault();
           var csrf_token  = "{{ csrf_token() }}";
           var id =$(this).attr('data-medical_id');
           var whichtr = $(this).closest("tr");

           swal({
               title: "Are you sure ?",
               text: "You want to delete this medical post ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
               .then((willDelete) => {
                   if (willDelete) {

                       $.ajax({
                           type: "GET",
                           url: "{{route('medical.destroy')}}",
                           data: {id:id,_token:csrf_token},
                           success: function(result) {

                               if(result.type == 'success'){

                                   whichtr.remove();
                                   swal('Medical successfully deleted','success');

                               }else{
                                   swal('Whoa!','Something Went Wrong.','error');
                               }
                           }
                       });

                   } else {
                       swal("Your medical posts is safe!");
                   }
               });

       });


       $('.medical_status').on('click',function(e){
           e.preventDefault();
           var csrf_token  = "{{ csrf_token() }}";
           var id = $(this).attr('data-medical_id');
           var status = $(this).attr('data-medical_status');

           swal({
               title: "Are you sure ?",
               text: "Are you sure you want to change status ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
               .then((willDelete) => {
                   if (willDelete) {

                       $.ajax({
                           type: "GET",
                           url: "{{route('medical.status')}}",
                           data: {id:id,status:status,_token:csrf_token},
                           success: function(result) {
                               if(result.type == 'success'){
                                   location.reload();
                               }
                           }
                       });

                   } else {
                       swal("Your status remian unchange !");
                   }
               });

       });

    </script>
@endsection