@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Medical Post</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="addMedicalForm" action="{{route('medical.store')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body"> 
                            <div class="form-group">
                                <label for="name">Medical Post Name</label>
                                <input type="text" class="form-control" name="post_name" placeholder="Enter Medical Post Name..">
                                 @if ($errors->has('post_name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('post_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Post Image</label>
                                <input type="file" name="post_image" accept="image/*">
                                @if ($errors->has('post_image'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('post_image') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Post Short Description</label>
                                <textarea class="ckeditor" required="" name="short_desc" ></textarea>
                                @if ($errors->has('short_desc'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('short_desc') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Post Detailed Description</label>
                                <textarea class="ckeditor" required="" name="lagre_desc" ></textarea>
                                @if ($errors->has('lagre_desc'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('lagre_desc') }}</strong>
                                    </span>
                                @endif
                            </div>

                        <div class="box-footer">
                            <a href="{{url('admin/medical')}}" class="btn btn-success">Cancel</a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')

<script type="text/javascript">

 $(function() {

     $('#addMedicalForm').validate({
         errorClass: 'text-danger',
         rules: {

             post_name:"required"
         },
         messages: {
             cat_name: "Please enter medical post name"
         },
         submitHandler: function(form) {
             form.submit();
         }
     });
 });

</script>

@stop