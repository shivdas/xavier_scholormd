@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Subcategory</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="addSubCategoryForm" action="{{route('subcat.store')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Category Name</label>
                                <select class="form-control" name="cat_id">
                                    <option value="" disabled selected>Select category option</option>
                                    @if(count($allcategory)>0)
                                        @foreach($allcategory as $val)
                                            <option value="{{$val->id}}">{{ucfirst($val->cat_name)}}</option>
                                        @endforeach
                                    @endif

                                </select>
                                @if ($errors->has('subcat_id'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('subcat_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Subcategory Name</label>
                                <input type="text" class="form-control" name="subcat_name" placeholder="Enter Subcategory Name..">
                                 @if ($errors->has('subcat_name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('subcat_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Sub Title</label>
                                <input type="text" class="form-control" name="sub_title" placeholder="Enter Sub Title..">
                                @if ($errors->has('sub_title'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('sub_title') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{url('admin/subcat')}}" class="btn btn-success">Cancel</a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')

<script type="text/javascript">

 $(function() {

     $('#addSubCategoryForm').validate({
         errorClass: 'text-danger',
         rules: {

             cat_id: "required",
             subcat_name:"required"
         },
         messages: {
             cat_id: "Please select category name",
             subcat_id: "Please select subcategory name",
             subcat_name: "Please enter Subcategory name"
         },
         submitHandler: function(form) {
             form.submit();
         }
     });
 });

</script>

@stop