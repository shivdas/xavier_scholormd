@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage All Subcategories</h3>
                    </div>
                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        <a href="{{route('subcat.create')}}" class="btn btn-info">Create Subcategory</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="subCategoryTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                {{--<th>Sr No.</th>--}}
                                <th>Category Name</th>
                                <th>Subcategory Name</th>
                                <th>Sub Title</th>
                                <th>Created On</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($allSubCategory)>0)
                            @foreach($allSubCategory as $subcategory)
                                <tr>
                                    {{--<td>{{$subcategory->id}}</td>--}}
                                    <td title="{{ucfirst($subcategory->cat_name)}}">{{str_limit(ucfirst($subcategory->cat_name),50)}}</td>
                                    <td title="{{ucfirst($subcategory->subcat_name)}}">{{str_limit(ucfirst($subcategory->subcat_name),50)}}</td>
                                    <td title="{{ucfirst($subcategory->sub_title)}}">{{str_limit(ucfirst($subcategory->sub_title),30)}}</td>
                                    <td>{{date("F jS, Y", strtotime($subcategory->created_at))}}</td>
                                    <td>
                                        @if($subcategory->is_active)
                                            <a data-subcat_id="{{$subcategory->id}}" data-subcat_status="{{$subcategory->is_active}}"    class="btn btn-primary subcat_status">Active</a>
                                        @else
                                            <a data-subcat_id="{{$subcategory->id}}" data-subcat_status="{{$subcategory->is_active}}"   class="btn btn-danger subcat_status">In-Active</a>
                                        @endif
                                        <a href="{{route('subcat.edit',['id'=>$subcategory->id])}}" class="btn btn-warning" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                        <a type="button" data-subcat_id="{{$subcategory->id}}" class="btn btn-danger delete_subcat" title="Delete"><i class="glyphicon glyphicon-edit"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')
    <script type="text/javascript">

       $('#subCategoryTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
           order: [ [0, 'desc'] ]
        });

       $('.delete_subcat').on('click',function(e){

           e.preventDefault();

           var csrf_token  = "{{ csrf_token() }}";
           var id =$(this).attr('data-subcat_id');
           var whichtr = $(this).closest("tr");

           swal({
               title: "Are you sure ?",
               text: "You want to delete this subcategory ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
           .then((willDelete) => {
               if (willDelete) {

                   $.ajax({
                       type: "GET",
                       url: "{{route('subcat.destroy')}}",
                       data: {id:id,_token:csrf_token},
                       success: function(result) {
                           if(result.type == 'success'){
                               whichtr.remove();
                               swal('Subcategory successfully deleted','success');
                           }else{
                               swal('Whoa!','Something Went Wrong.','error');
                           }
                       }
                   });

               } else {
                   swal("Your subcategory is safe!");
               }
           });

       });


       $('.subcat_status').on('click',function(e){
           e.preventDefault();
           var csrf_token  = "{{ csrf_token() }}";
           var id = $(this).attr('data-subcat_id');
           var status = $(this).attr('data-subcat_status');
            console.log(id);
            console.log(status);
           swal({
               title: "Are you sure ?",
               text: "Are you sure you want to change status ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
               .then((willDelete) => {
                   if (willDelete) {

                       $.ajax({
                           type: "GET",
                           url: "{{route('subcat.status')}}",
                           data: {id:id,status:status,_token:csrf_token},
                           success: function(result) {
                               if(result.type == 'success'){
                                   location.reload();
                               }
                           }
                       });

                   } else {
                       swal("Your status remian unchange !");
                   }
               });

       });


    </script>
@endsection