@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage All {{$categories->cat_name}}</h3>
                    </div>
                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        <a href="{{route('category.index')}}" class="btn btn-info">Back To Category</a>
                    </div>

                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        <a href="{{route('places.create',['id'=>$categories->id])}}" class="btn btn-info">Add {{$categories->cat_name}}</a>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="placeTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Address</th>
                                <th>Website</th>
                                <th>Category</th>
                                <th>Actions</th>  
                                <th>Actions</th>  
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($places as $place)
                                <tr>
                                    <td>{{$place->id}}</td>
                                    <td>{{$place->name}}</td>
                                    <td>{{$place->contact_number}}</td>
                                    <td>{{$place->address}}</td>
                                    <td>{{$place->website}}</td>
                                    <td>{{$place->category->cat_name}}</td>
                                    <td><a href="{{route('places.edit',['id'=>$place->id])}}" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i> Edit</a></td>
                                    <td><a href="{{route('places.destroy',['id'=>$place->id,'category_id'=>$place->category->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this record ?')"><i class="glyphicon glyphicon-trash"></i> Delete</a></td> 
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Sr No.</th>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Address</th>
                                <th>Website</th>
                                <th>Category</th>
                                <th>Actions</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')
    <script type="text/javascript">
       $('#placeTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
        });
    </script>
@endsection