@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Place Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="editPlaceForm" action="{{route('places.update',[$place->id])}}" method="post" enctype="multipart/form-data">
                    {{method_field('PUT')}}
                        {{csrf_field()}}
                        <div class="box-body">

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Enter Place Name.." value="{{$place->name}}">
                                 @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="contact_number">Contact Number</label>
                                <input type="text" class="form-control" name="contact_number" placeholder="Enter Place Contact Number.." value="{{$place->contact_number}}">
                                 @if ($errors->has('contact_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Enter Place Address.." value="{{$place->address}}">
                                 @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="website">Website</label>
                                <input type="text" class="form-control" name="website" placeholder="Enter Place Website.." value="{{$place->website}}">
                                 @if ($errors->has('website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" placeholder="Description" id="page_desc">{{$place->description}}</textarea>
                                 @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                                <span id="desc_error"></span>
                            </div>

                        </div>
                        <input type="hidden" name="place_address" id="place_address" value="{{$place->address}}">
                        <input type="hidden" name="lat" id="lat" value="{{$place->lat}}">
                        <input type="hidden" name="lng" id="lng" value="{{$place->lng}}">
                        <input type="hidden" name="category_id" value="{{$place->category_id}}">
                            <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')
<script type="text/javascript">
 $(function () {
    CKEDITOR.replace('page_desc');
  });

 jQuery.validator.addMethod("phoneUS", function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
                  phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
        }, "Invalid contact number");

     $('#editPlaceForm').validate({
                errorClass: 'text-danger',
                rules: {
                    'name': {
                        required: true
                    },
                    'contact_number': {
                        required: true,
                        phoneUS:true
                    },
                    'address':{
                        required:true,
                    },
                    'website':{
                        required:true,
                        url:true
                    },   
                },
                messages: {
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });


            var input = document.getElementById('address');
            var options = {
                    componentRestrictions: {country: ['us']}
                };
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addDomListener(input, 'keydown', function(event) {
                        if (event.keyCode === 13) {
                            event.preventDefault();
                        }
                    });
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                var address = place.formatted_address;
                $("#place_address").val(address);
                $("#lat").val(place.geometry.location.lat());
                $("#lng").val(place.geometry.location.lng());
            });
           
</script>

@stop