@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage All Enquiries</h3>
                    </div>
                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        {{--<a href="#" class="btn btn-info">Create Consultant</a>--}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="enquirieTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                {{--<th>Sr No.</th>--}}
                                <th>User Nmae</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Comments</th>
                                <th>Consultant Assigned</th>
                                <th>Consultant Assigned On</th>
                                <th>Created On</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($allEnquirie)>0)
                            @foreach($allEnquirie as $enquirie)
                                <tr>
                                    <td>{{ucfirst($enquirie->user_name)}}</td>
                                    <td>{{$enquirie->phone}}</td>
                                    <td>{{$enquirie->email}}</td>
                                    <td title="{{ucfirst($enquirie->description)}}">{{str_limit(ucfirst($enquirie->description),30)}}</td>
                                    <td>{{!empty($enquirie->expert_name) ? ucfirst($enquirie->expert_name) : ''}}</td>
                                    <td>{{!empty($enquirie->consultant_assigne_on) ? date("F jS, Y", strtotime($enquirie->consultant_assigne_on)) : ''}}</td>
                                    <td>{{date("F jS, Y", strtotime($enquirie->created_at))}}</td>
                                    <td>
                                        <a data-enq_id="{{$enquirie->id}}" data-toggle="modal" data-target="#myModal" title="Assigne Consultant"  class="btn btn-primary assign_to">Assigne To</a>
                                        <a type="button" data-enq_id="{{$enquirie->id}}" class="btn btn-danger delete_enq" title="Delete"><i class="glyphicon glyphicon-edit"></i> Delete</a>
                                    </td>

                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>


                <!-- Modal:Start -->
                <div class="modal fade" id="myModal" role="dialog">
                    <form role="form" id="consultantForm" action="{{route('enquirie.assignConsultant')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Assigne Consultant</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" id="enq_id" name="enq_id" value="">
                                    <label for="name">Consultant Name</label>
                                    <select class="form-control" name="consultant_id">
                                        <option value="" disabled selected>Select consultant name</option>
                                        @if(count($allConsutants)>0)
                                            @foreach($allConsutants as $val)
                                                <option value="{{$val->id}}">{{ucfirst($val->firstname.' '.$val->lastname)}}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                    @if ($errors->has('cat_id'))
                                        <span class="text-danger">
                                        <strong>{{ $errors->first('consultant_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                <button  id="assign_consutant" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <!-- Modal:End -->
            </div>


            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')

    <script type="text/javascript">


        $('#enquirieTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
           order: [ [0, 'desc'] ]
        });


        $(function() {

            $('#consultantForm').validate({
                errorClass: 'text-danger',
                rules: {

                    consultant_id: "required",
                },
                messages: {
                    consultant_id: "Please select consultant name",
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });

       $('.delete_enq').on('click',function(e){

           e.preventDefault();

           var csrf_token  = "{{ csrf_token() }}";
           var id =$(this).attr('data-enq_id');
           var whichtr = $(this).closest("tr");

           swal({
               title: "Are you sure ?",
               text: "You want to delete this enquirie ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
           .then((willDelete) => {
               if (willDelete) {

                   $.ajax({
                       type: "GET",
                       url: "{{route('enquirie.destroy')}}",
                       data: {id:id,_token:csrf_token},
                       success: function(result) {
                           if(result.type == 'success'){
                               whichtr.remove();
                               swal('Enquirie successfully deleted','success');
                           }else{
                               swal('Whoa!','Something Went Wrong.','error');
                           }
                       }
                   });

               } else {
                   swal("Your chapter is safe!");
               }
           });

       });

        $('.assign_to').on('click',function(e){
            var enq_id =$(this).attr('data-enq_id');
            $('#enq_id').val(enq_id);

        });


    </script>
@endsection