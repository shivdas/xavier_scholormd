@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage All Facts</h3>
                    </div>
                    <div class="addDealer" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        <a href="{{route('facts.create')}}" class="btn btn-info">Create Fact</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="factsTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>Fact Name</th>
                                <th>Description</th>
                                {{--<th>Publish Date</th>--}}
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($allFacts as $fact)
                                <tr>
                                    <td>{{$fact->id}}</td>
                                    <td>{{$fact->name}}</td>
                                    <td>{!!$fact->description!!}</td>
                                   {{-- <td>{{Carbon\Carbon::parse($fact->fact_date)->format('m-d-Y')}}</td>--}}
                                    <td>
                                    @if($fact->is_active)
                                        <a href="{{route('facts.status',['id'=>$fact->id,'status'=>$fact->is_active])}}" onclick="return confirm('Are you sure you want to change status ?')" class="btn btn-primary">Active</a>
                                    @else
                                        <a href="{{route('facts.status',['id'=>$fact->id,'status'=>$fact->is_active])}}" onclick="return confirm('Are you sure you want to change status ?')" class="btn btn-danger">In-Active</a>
                                    @endif
                                    </td>
                                    <td><a href="{{route('facts.edit',['id'=>$fact->id])}}" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <a href="{{route('facts.delete',['id'=>$fact->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this record ?')"><i class="glyphicon glyphicon-trash"></i> Delete</a></td>
                                    
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Sr No.</th>
                                <th>Fact Name</th>
                                <th>Description</th>
                              {{--<th>Publish Date</th>--}}
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')
    <script type="text/javascript">
    $('#factsTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
        });
    </script>
@endsection