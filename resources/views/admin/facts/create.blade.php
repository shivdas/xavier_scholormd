@extends('layouts.master')
@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Post Details</h3>
                    </div>
                    <form role="form" id="factAdd" action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Enter Post Title.." value="{{old('name')}}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="button" id = "add_more" class="btn btn-primary">Add Section</button>
                        </div>
                </div>
            </div>
        </div>
        <div id ="section_details" >
            <div  class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Section Details</h3>
                        </div>

                        <div class="box-body" id="post_box">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name[]" placeholder="Enter Post Title.." id = "page_title" value="{{old('name')}}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="ckeditor" name="description[]" placeholder="Description" id="page_desc">{{old('description')}}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                           <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                @endif
                                <span id="desc_error"></span>
                            </div>

                        </div>

                        <div id="new_div"></div>




                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </section>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#facts_date').datetimepicker({format: 'MM/DD/YYYY',minDate:moment(),date:new Date(),allowInputToggle: true});
        })
    </script>
    <script type="text/javascript">

        /*  function exe_ckeditor(id){
             CKEDITOR.replace('page_desc'+id);

         } */
        //exe_ckeditor(1);
        //CKEDITOR.replace('page_desc');
        $('#factAdd').validate({
            errorClass: 'text-danger',
            errorPlacement:function(error,element){
                if(element.attr('name') == 'description'){
                    error.appendTo('#desc_error');
                }else{
                    error.insertAfter(element);
                }
            },
            ignore: [],
            debug: false,
            rules: {
                'name': {
                    required: true
                },
                'fact_date':{
                    required:true,
                    date:true
                },
            },
            messages: {
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        var  uniqueId = 1;
        $('#add_more').click(function(e){ //on add input button click
            /*  var html  = $("#post_box").html();
             $('#section_details').append(html);
             CKEDITOR.replace('page_desc'); */

            //var copy = $("#post_box").html();
            var copy = $("#post_box").clone(true);



            uniqueId++;
            var formId = 'page_desc' + uniqueId;
            copy.find('.ckeditor').attr('id', formId );

            //console.log(copy);

            $('#new_div').append(copy);

            /*$('#' + formId).find('textarea').each(function(){
                $(this).attr('id', $(this).attr('id') + uniqueId);
            });*/
            CKEDITOR.replace('page_desc'+uniqueId);
            //$(".box-body").show().css('visibility','normal');
            //   exe_ckeditor(uniqueId);
        });
    </script>

@stop