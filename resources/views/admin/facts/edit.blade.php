@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Facts Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="factAdd" action="{{route('facts.update',[$facts->id])}}" method="post" enctype="multipart/form-data">
                        {{method_field('PUT')}}
                        {{csrf_field()}}
                        <div class="box-body">

                           {{-- <label for="date">Date</label>
                            <div class="form-group" id="facts_date">
                                <input type='text' class="form-control" id="fact_date"  name="fact_date" data-date="{{$facts->fact_date}}"/>
                            </div>--}}
                            
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Enter Fact Name.." value="{{$facts->name}}">
                                 @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" placeholder="Description" id="page_desc">{{$facts->description}}</textarea>
                                 @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                                <span id="desc_error"></span>
                            </div>
                            
                        </div>
                            <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')
<script type="text/javascript">
 $(function () {
    CKEDITOR.replace('page_desc');
  });

    $('#facts_date').datetimepicker({format: 'MM/DD/YYYY',allowInputToggle: true,defaultDate : $('#fact_date').attr('data-date')});

     $('#factAdd').validate({
                errorClass: 'text-danger',
                errorPlacement:function(error,element){
                    if(element.attr('name') == 'description'){
                        error.appendTo('#desc_error');
                    }else{
                        error.insertAfter(element);
                    }
                },
                ignore: [],
                debug: false,
                rules: {
                    'name': {
                        required: true
                    },
                    'fact_date':{
                        required:true,
                        date:true
                    },
                },
                messages: {
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
</script>

@stop