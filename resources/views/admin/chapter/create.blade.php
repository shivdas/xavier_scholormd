@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Chapter</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="addChapterForm" action="{{route('chapter.store')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Category Name</label>
                                <select class="form-control" name="cat_id" id="category_name">
                                    <option value="" disabled selected>Select category option</option>
                                    @if(count($allcategory)>0)
                                        @foreach($allcategory as $val)
                                            <option value="{{$val->id}}">{{ucfirst($val->cat_name)}}</option>
                                        @endforeach
                                    @endif

                                </select>
                                @if ($errors->has('cat_id'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('cat_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Subcategory Name</label>
                                <select class="form-control" name="subcat_id" id="subcategory_name">
                                </select>
                                @if ($errors->has('subcat_id'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('subcat_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Chapter Name</label>
                                <input type="text" class="form-control" name="chapter_name" placeholder="Enter Chapter Name..">
                                @if ($errors->has('chapter_name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('chapter_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Sub Title</label>
                                <input type="text" class="form-control" name="sub_title" placeholder="Enter Sub Title..">
                                @if ($errors->has('sub_title'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('sub_title') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Description</label>
                                <textarea class="ckeditor" required="" name="chapter_description" ></textarea>
                                @if ($errors->has('chapter_description'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('chapter_description') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group " >
                                    <table class="table parent_table"  style="display: none;">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <label for="name">Section Title</label>
                                                <input type="text" class="form-control"  name="section_title_1" placeholder="Enter Section Title..">
                                                <label for="description">Section Description</label>
                                                <textarea class="ckeditor" required=""  name="description_1" ></textarea>
                                            </td>
                                            <td><a href="javascript:void(0)" class="btn btn-danger remove_more">Remove</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <a href="javascript:void(0)" class="btn btn-success add_more right" style="float: right;">Add Section</a>
                            </div>

                            <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{url('admin/chapter')}}" class="btn btn-success">Cancel</a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')
    <script src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">



 $(function() {

     $('#addChapterForm').validate({
         errorClass: 'text-danger',
         rules: {

             cat_id: "required",
             subcat_id: "required",
             chapter_name:"required"
         },
         messages: {
             cat_id: "Please select category name",
             subcat_id: "Please select subcategory name",
             chapter_name: "Please enter chapter name"
         },
         submitHandler: function(form) {
             form.submit();
         }
     });
 });

 //clone div:START
     var REMOVE = '';
     var i=1;
    var rowCount = 1;
     $(document).ready(function () {

         $('.add_more').click(function () {

             var is_visible = $('.parent_table').is(":visible");
             if(is_visible == false){
                $('.parent_table').show();
                  /*$(".section_title_1").prop('disabled', true);
                  $(".description_1").prop('disabled', true);*/
                return false;
             }
             var oneplus=i+1;
             var tr_object = $('tbody').find('tr:first').clone();
             $(tr_object).find('input[name="section_title_1"]').attr("name", "section_title_"+oneplus+"");
             $(tr_object).find('textarea[name="description_1"]').attr("name", "description_"+oneplus+"");
             $(tr_object).find('td:last').html('<a href="javascript:void(0)" class="btn btn-danger remove_more">Remove</a>');
             $('tbody').append(tr_object);
             CKEDITOR.replace("description_"+oneplus+"");
             $('#cke_description_1').each(function() {
                 var $ids = $('[id=' + this.id + ']');
                 if ($ids.length > 1) {
                     $ids.not(':first').remove();
                 }
             });
             i=i+1;
             oneplus++;
         });

         $(document).on('click', '.remove_more', function () {

             rowCount = $('.parent_table tr').length;

             var id = $(this).closest('tr').find('.id').val();
             if (id != '') {
                 if (REMOVE != '') {
                     REMOVE = REMOVE + ',' + id;
                 } else {
                     REMOVE = id;
                 }
                 $('#id').val(REMOVE);
             }
             if(rowCount == 1){
                 /*$(".section_title_1").prop('disabled', false);
                 $(".description_1").prop('disabled', false);*/
                 $('.parent_table').hide();
                 return false;
             }
             $(this).closest('tr').remove();
             rowCount = rowCount -1;
         });
     });

 //clone div:END

 $('#category_name').change(function(){

     var category_id = $(this).val();
     $('#subcategory_name').empty();

     $.ajax({
         url:"{{route('chapter.subCatList')}}",
         method: 'GET',
         data: {category_id: category_id},
         dataType: 'json',
         success: function(response){

             $('#subcategory_name').append('<option value="" disabled selected>Select Subcategory Name</option>');

            if(response.length > 0){
                $.each(response,function(index,data){
                    $('#subcategory_name').append('<option value="'+data['id']+'">'+data['subcat_name']+'</option>');
                });
            }

         }
     });
 });

</script>

@stop