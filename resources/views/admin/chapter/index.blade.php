@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage All Chapters</h3>
                    </div>
                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        <a href="{{route('chapter.create')}}" class="btn btn-info">Create Chapter</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="chapterTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Chapter Name</th>
                                <th>Category Name</th>
                                <th>Subcategory Name</th>
                                <th>Sub Title</th>
                                <th>Created On</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($allChapter)>0)
                            @foreach($allChapter as $chapter)
                                <tr>
                                    <td>{{$chapter->id}}</td>
                                    <td title="{{ucfirst($chapter->chapter_name)}}">{{str_limit(ucfirst($chapter->chapter_name),20)}}</td>
                                    <td title="{{ucfirst($chapter->cat_name)}}">{{str_limit(ucfirst($chapter->cat_name),20)}}</td>
                                    <td title="{{ ucfirst($chapter->subcat_name) }}">{{str_limit(ucfirst($chapter->subcat_name),20)}}</td>
                                    <td title="{{ucfirst($chapter->sub_title)}}">{{str_limit(ucfirst($chapter->sub_title),20)}}</td>
                                    <td>{{date("F jS, Y", strtotime($chapter->created_at))}}</td>
                                    <td>
                                        @if($chapter->is_active)
                                            <a data-chapter_id="{{$chapter->id}}" data-chapter_status="{{$chapter->is_active}}"    class="btn btn-primary chapter_status">Active</a>
                                        @else
                                            <a data-chapter_id="{{$chapter->id}}" data-chapter_status="{{$chapter->is_active}}"   class="btn btn-danger chapter_status">In-Active</a>
                                        @endif
                                        <a href="{{route('chapter.edit',['id'=>$chapter->id])}}" class="btn btn-warning" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                        <a type="button" data-chapter_id="{{$chapter->id}}" class="btn btn-danger delete_chapter" title="Delete"><i class="glyphicon glyphicon-edit"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')

    <script type="text/javascript">

       $('#chapterTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
           order: [ [0, 'desc'] ],

        });


       $('.delete_chapter').on('click',function(e){

           e.preventDefault();

           var csrf_token  = "{{ csrf_token() }}";
           var id =$(this).attr('data-chapter_id');
           var whichtr = $(this).closest("tr");

           swal({
               title: "Are you sure ?",
               text: "You want to delete this chapter ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
           .then((willDelete) => {
               if (willDelete) {

                   $.ajax({
                       type: "GET",
                       url: "{{route('chapter.destroy')}}",
                       data: {id:id,_token:csrf_token},
                       success: function(result) {
                           if(result.type == 'success'){
                               whichtr.remove();
                               swal('Chapter successfully deleted','success');
                           }else{
                               swal('Whoa!','Something Went Wrong.','error');
                           }
                       }
                   });

               } else {
                   swal("Your chapter is safe!");
               }
           });

       });

       $('.chapter_status').on('click',function(e){
           e.preventDefault();
           var csrf_token  = "{{ csrf_token() }}";
           var id = $(this).attr('data-chapter_id');
           var status = $(this).attr('data-chapter_status');

           swal({
               title: "Are you sure ?",
               text: "Are you sure you want to change status ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
               .then((willDelete) => {
                   if (willDelete) {

                       $.ajax({
                           type: "GET",
                           url: "{{route('chapter.status')}}",
                           data: {id:id,status:status,_token:csrf_token},
                           success: function(result) {
                               if(result.type == 'success'){
                                   location.reload();
                               }
                           }
                       });

                   } else {
                       swal("Your status remian unchange !");
                   }
               });

       });


    </script>
@endsection