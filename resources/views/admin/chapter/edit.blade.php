@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Chapter</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="editChapterForm" action="{{route('chapter.update')}}" method="post" enctype="multipart/form-data">
                        {{method_field('PUT')}}
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <input type="hidden" name="chapter_id" value="{{$chapter_data->id}}">
                                <input type="hidden" name="subcat_id" id="subcat_id" value="{{$chapter_data->subcat_id}}">
                                <input id="total_sections" type="hidden" name="total_sections" value="{{count((array)$sections)}}">
                                <label for="name">Category Name</label>
                                <select class="form-control" name="cat_id" id="category_name">
                                    <option value="" disabled selected>Select category option</option>
                                    @if(count($allcategory)>0)
                                        @foreach($allcategory as $val)
                                            <option value="{{$val->id}}" {{ ($chapter_data->cat_id == $val->id ? "selected":"") }} >{{ucfirst($val->cat_name)}}</option>
                                        @endforeach
                                    @endif

                                </select>
                                @if ($errors->has('cat_id'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('cat_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Subcategory Name</label>
                                <select class="form-control" name="subcat_id" id="subcategory_name">
                                    {{--<option value="" disabled selected>Select subcategory option</option>
                                    @if(count($allsubcategory)>0)
                                        @foreach($allsubcategory as $vale)
                                            <option value="{{$vale->id}}" {{ ($chapter_data->subcat_id == $vale->id ? "selected":"") }}>{{ucfirst($vale->subcat_name)}}</option>
                                        @endforeach
                                    @endif--}}

                                </select>
                                @if ($errors->has('subcat_id'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('subcat_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Chapter Name</label>
                                <input type="text" class="form-control" name="chapter_name" placeholder="Enter Chapter_name Name.." value="{{$chapter_data->chapter_name}}">
                                @if ($errors->has('chapter_name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('chapter_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Sub Title</label>
                                <input type="text" class="form-control" name="sub_title" placeholder="Enter Sub Title.." value="{{$chapter_data->sub_title}}">
                                @if ($errors->has('sub_title'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('sub_title') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="ckeditor" name="chapter_description" placeholder="Description" id="page_desc">{{$desc_section}}</textarea>
                                @if ($errors->has('chapter_description'))
                                    <span class="help-block">
                                           <strong>{{ $errors->first('chapter_description') }}</strong>
                                        </span>
                                @endif
                                <span id="desc_error"></span>
                            </div>
                            <div class="form-group">
                                <table class="table parent_table"  style="display: none;">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <label for="name">Section Title</label>
                                            <input type="text" class="form-control" name="section_title_1" placeholder="Enter Section Title..">
                                            <label for="description">Section Description</label>
                                            <textarea class="ckeditor" required="" name="description_1" ></textarea>
                                        </td>
                                        <td><a href="javascript:void(0)" class="btn btn-danger remove_more">Remove</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <a  class="btn btn-success add_more right" onclick="add_more(1);"  style="float: right;">Add Section</a>
                            </div>

                            <!-- /.box-body -->
                            <div class="box-footer">
                                <a href="{{url('admin/chapter')}}" class="btn btn-success">Cancel</a>
                                <button type="submit" class="btn btn-primary">Submit</button>

                            </div>
                            </div>
                    </form>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')

    <script type="text/javascript">

        $(function () {
            $("#category_name").change();
        });

        $(function() {

            $('#editChapterForm').validate({
                errorClass: 'text-danger',
                rules: {

                    cat_id: "required",
                    subcat_id: "required",
                    chapter_name:"required"
                },
                messages: {
                    cat_id: "Please select category name",
                    subcat_id: "Please select subcategory name",
                    chapter_name: "Please enter chapter name"
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });

        $(function () {

            var total_sections = parseInt($('#total_sections').val());
            var sections_data = <?php echo json_encode($sections); ?>;
            console.log(total_sections);
            if(total_sections == 1){
                $('.parent_table').show();
                $('.parent_table').find("input,textarea").attr("disabled",false);

            }else if(total_sections > 1){
                for(var increment = 1; increment  <= total_sections; increment++) {//add dynmic section
                    add_more(2);
                }
            }

            $.each( sections_data, function( key, value ) {//append values to input and textarea
                var temp = key+1;
                $("input[name=section_title_"+temp+"]").val(value.section_title);
                $("textarea[name=description_"+temp+"]").val(value.description);
            });

        });

        //clone div:START
        var REMOVE = '';
        var i=1;
        var rowCount = 1;

        function add_more(mode){
            var is_visible = $('.parent_table').is(":visible");
            if(is_visible == false){
                $('.parent_table').show();
                $('.parent_table').find("input,textarea").attr("disabled",false);
                /*$(".section_title_1").prop('disabled', true);
                $(".description_1").prop('disabled', true);*/
                return false;
            }

            var oneplus=i+1;
            var tr_object = $('tbody').find('tr:first').clone();
            $(tr_object).find('input[name="section_title_1"]').attr("name", "section_title_"+oneplus+"");
            $(tr_object).find('textarea[name="description_1"]').attr("name", "description_"+oneplus+"");
            $(tr_object).find('td:last').html('<a href="javascript:void(0)" class="btn btn-danger remove_more">Remove</a>');
            if(mode == 1){
                $(tr_object).find("input[name=section_title_"+oneplus+"]").val('');
                $(tr_object).find("textarea[name=description_"+oneplus+"]").val('');
            }
            $('tbody').append(tr_object);
            CKEDITOR.replace("description_"+oneplus+"");
            $('#cke_description_1').each(function() {
                var $ids = $('[id=' + this.id + ']');
                if ($ids.length > 1) {
                    $ids.not(':first').remove();
                }
            });
            i=i+1;
            oneplus++;
        };

        $(document).on('click', '.remove_more', function () {

            rowCount = $('.parent_table tr').length;

            var id = $(this).closest('tr').find('.id').val();
            if (id != '') {
                if (REMOVE != '') {
                    REMOVE = REMOVE + ',' + id;
                } else {
                    REMOVE = id;
                }
                $('#id').val(REMOVE);
            }
            if(rowCount == 1){
                /*$(".section_title_1").prop('disabled', false);
                $(".description_1").prop('disabled', false);*/
                $('.parent_table').hide();
                $('.parent_table').find("input,textarea").attr("disabled",true);
                return false;
            }
            $(this).closest('tr').remove();
            rowCount = rowCount -1;
        });
        //clone div:END



        $('#category_name').change(function(){

            var category_id = $(this).val();
            $('#subcategory_name').empty();
            var subcat_id = $('#subcat_id').val();

            $.ajax({
                url:"{{route('chapter.subCatList')}}",
                method: 'GET',
                data: {category_id: category_id},
                dataType: 'json',
                success: function(response){
                    $('#subcategory_name').append('<option value="" disabled selected>Select Subcategory Name</option>');

                    if(response.length > 0){
                        $.each(response,function(index,data){
                            var selected = '';
                            if(subcat_id == data['id']){
                                selected = 'selected';
                            }

                            $('#subcategory_name').append('<option value="'+data['id']+'" '+selected+'>'+data['subcat_name']+'</option>');
                        });
                    }

                }
            });
        });

    </script>

@stop