@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All User Selections</h3>
                    </div>
                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        {{--<a href="#" class="btn btn-info">Create Consultant</a>--}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="selectionTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Chapter Name</th>
                                <th>Selected Text</th>
                                <th>Created On</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($allSelection)>0)
                            @foreach($allSelection as $selection)
                                <tr>
                                    <td>{{ucfirst($selection->user_name)}}</td>
                                    <td>{{ucfirst($selection->chapter_name)}}</td>
                                    <td title="{{ucfirst($selection->selected_text)}}">{{str_limit(ucfirst($selection->selected_text),30)}}</td>
                                    <td>{{date("F jS, Y", strtotime($selection->created_at))}}</td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')

    <script type="text/javascript">

       $('#selectionTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
           order: [ [0, 'desc'] ]
        });

    </script>
@endsection