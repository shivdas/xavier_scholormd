@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Users Notes > {{ !empty($userName->firstname) ? ucfirst($userName->firstname.' '.$userName->lastname) :'' }}</h3>
                    </div>
                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        {{--<a href="{{route('user.create')}}" class="btn btn-info">Create User</a>--}}
                        {{--<a href="{{route('user.index')}}" class="btn btn-info"><i class="glyphicon glyphicon-chevron-left"></i>Back</a>--}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="noteTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>User Notes</th>
                                <th>Created On</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($userNotes)>0)
                            @foreach($userNotes as $note)
                                <tr>
                                    <td>{{$note->id}}</td>
                                    <td title="{{ucfirst($note->notes)}}">{{str_limit(ucfirst($note->notes),150)}}</td>
                                    <td>{{ date('F j, Y, g:i a', strtotime( $note->created_at) ) }}</td>
                                    {{--<td>
                                        <a href="{{route('user.edit',['id'=>$note->id])}}" class="btn btn-warning" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                        <a type="button" data-note_id="{{$note->id}}" class="btn btn-danger delete_note" title="Delete"><i class="glyphicon glyphicon-edit"></i> Delete</a>
                                    </td>--}}
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')

    <script type="text/javascript">

       $('#noteTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
           order: [ [0, 'desc'] ]
        });


       $('.delete_note').on('click',function(e){

           e.preventDefault();

           var csrf_token  = "{{ csrf_token() }}";
           var id =$(this).attr('data-note_id');
           var whichtr = $(this).closest("tr");

           swal({
               title: "Are you sure ?",
               text: "You want to delete this user ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
           .then((willDelete) => {
               if (willDelete) {

                   $.ajax({
                       type: "GET",
                       url: "{{route('user.destroy')}}",
                       data: {id:id,_token:csrf_token},
                       success: function(result) {
                           if(result.type == 'success'){
                               whichtr.remove();
                               swal('User successfully deleted','success');
                           }else{
                               swal('Whoa!','Something Went Wrong.','error');
                           }
                       }
                   });

               } else {
                   swal("Your user is safe!");
               }
           });

       });



    </script>
@endsection