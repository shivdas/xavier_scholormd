@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="editUserForm" action="{{route('user.update')}}" method="post" enctype="multipart/form-data">
                    {{method_field('PUT')}}
                        {{csrf_field()}}
                        <div class="box-body">

                            <div class="form-group">
                                <label for="name">Profile Image</label>
                                <input type="file" name="profile_image" accept="image/*">
                                <img src="{{asset('storage/images/profile_pic/'.$user_data->profile_pic)}}" alt="Profile Image" height="200" width="200">
                                @if ($errors->has('profile_image'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('profile_image') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="hidden" name="user_id" value="{{$user_data->id}}">
                                <label for="name">User Firstname</label>
                                <input type="text" class="form-control" name="firstname" placeholder="Enter User Firstname.." value="{{$user_data->firstname}}">
                                @if ($errors->has('firstname'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">User Lastname</label>
                                <input type="text" class="form-control" name="lastname" placeholder="Enter User Lastname.." value="{{$user_data->lastname}}">
                                @if ($errors->has('lastname'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Phone Number</label>
                                <input type="number" class="form-control" name="phone" placeholder="Enter User Phone No.." value="{{$user_data->phone}}">
                                @if ($errors->has('phone'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Enter User Email.." value="{{$user_data->email}}">
                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Specialty</label>
                                <input type="text" class="form-control" name="specialty" placeholder="Enter User Specialty.." value="{{$user_data->specialty}}">
                                @if ($errors->has('specialty'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('specialty') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Location</label>
                                <input type="text" class="form-control" name="location" placeholder="Enter User Location.." value="{{$user_data->location}}">
                                @if ($errors->has('location'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{url('admin/user')}}" class="btn btn-success">Cancel</a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')

<script type="text/javascript">

$(function() {

    $('#editUserForm').validate({

        errorClass: 'text-danger',
        rules: {
            firstname: "required",
            phone:"required",
            subject_expert:"required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            firstname: "Please enter user firstname name",
            phone: "Please enter user phone number",
            email: "Please enter valid user email address",
            subject_expert: "Please enter user subject expert"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});

</script>

@stop