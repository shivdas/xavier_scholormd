@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage All Users</h3>
                    </div>
                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        {{--<a href="{{route('user.create')}}" class="btn btn-info">Create User</a>--}}
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="userTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                {{--<th>Sr No.</th>--}}
                                <th>User Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Speciality</th>
                                <th>Province</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($allUser)>0)
                            @foreach($allUser as $user)
                                <tr>
                                    {{--<td>{{$user->id}}</td>--}}
                                    <td>{{ucfirst($user->firstname.' '.$user->lastname)}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{str_limit(ucfirst($user->speciality),20)}}</td>
                                    <td>{{str_limit(ucfirst($user->province),20)}}</td>
                                    <td>
                                        @if($user->active)
                                            <a data-user_id="{{$user->id}}" data-user_status="{{$user->active}}"    class="btn btn-primary user_status">Active</a>
                                        @else
                                            <a data-user_id="{{$user->id}}" data-user_status="{{$user->active}}"   class="btn btn-danger user_status">In-Active</a>
                                        @endif
                                        <a href="{{route('user.edit',['id'=>$user->id])}}" class="btn btn-warning" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                        <a href="{{route('user.show_notes',['id'=>$user->id])}}" class="btn btn-info" title="Edit" target="_blank"><i class="glyphicon glyphicon-comment"></i> Notes</a>
                                        <a type="button" data-user_id="{{$user->id}}" class="btn btn-danger delete_user" title="Delete"><i class="glyphicon glyphicon-edit"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')

    <script type="text/javascript">

       $('#userTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
           order: [ [0, 'desc'] ]
        });


       $('.delete_user').on('click',function(e){

           e.preventDefault();

           var csrf_token  = "{{ csrf_token() }}";
           var id =$(this).attr('data-user_id');
           var whichtr = $(this).closest("tr");

           swal({
               title: "Are you sure ?",
               text: "You want to delete this user ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
           .then((willDelete) => {
               if (willDelete) {

                   $.ajax({
                       type: "GET",
                       url: "{{route('user.destroy')}}",
                       data: {id:id,_token:csrf_token},
                       success: function(result) {
                           if(result.type == 'success'){
                               whichtr.remove();
                               swal('User successfully deleted','success');
                           }else{
                               swal('Whoa!','Something Went Wrong.','error');
                           }
                       }
                   });

               } else {
                   swal("Your user is safe!");
               }
           });

       });

       $('.user_status').on('click',function(e){
           e.preventDefault();
           var csrf_token  = "{{ csrf_token() }}";
           var id = $(this).attr('data-user_id');
           var status = $(this).attr('data-user_status');

           swal({
               title: "Are you sure ?",
               text: "Are you sure you want to change status ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
               .then((willDelete) => {
                   if (willDelete) {

                       $.ajax({
                           type: "GET",
                           url: "{{route('user.status')}}",
                           data: {id:id,status:status,_token:csrf_token},
                           success: function(result) {
                               if(result.type == 'success'){
                                   location.reload();
                               }
                           }
                       });

                   } else {
                       swal("Your status remian unchange !");
                   }
               });

       });


    </script>
@endsection