@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="x_title">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('class')}} alert-dismissible fade in" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage All Consultant</h3>
                    </div>
                    <div class="addCategory" style="float:right;margin-bottom: 6px;margin-right: 18px;">
                        <a href="{{route('consultant.create')}}" class="btn btn-info">Create Consultant</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="consultantTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                {{--<th>Sr No.</th>--}}
                                <th>Consultant Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Subject Expert</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($allConsultant)>0)
                            @foreach($allConsultant as $consultant)
                                <tr>
                                    {{--<td>{{$consultant->id}}</td>--}}
                                    <td>{{ucfirst($consultant->firstname.' '.$consultant->lastname)}}</td>
                                    <td>{{$consultant->phone}}</td>
                                    <td>{{$consultant->email}}</td>
                                    <td>{{ucfirst($consultant->subject_expert)}}</td>
                                    <td>
                                        @if($consultant->is_active)
                                            <a data-consultant_id="{{$consultant->id}}" data-consultant_status="{{$consultant->is_active}}"    class="btn btn-primary consultant_status">Active</a>
                                        @else
                                            <a data-consultant_id="{{$consultant->id}}" data-consultant_status="{{$consultant->is_active}}"   class="btn btn-danger consultant_status">In-Active</a>
                                        @endif
                                        <a href="{{route('consultant.edit',['id'=>$consultant->id])}}" class="btn btn-warning" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                        <a type="button" data-consultant_id="{{$consultant->id}}" class="btn btn-danger delete_consultant" title="Delete"><i class="glyphicon glyphicon-edit"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
    {{csrf_field()}}
@endsection
@section('js')

    <script type="text/javascript">

       $('#consultantTable').DataTable({
            responsive: true,
            "pageLength": 10,
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
           order: [ [0, 'desc'] ]
        });


       $('.delete_consultant').on('click',function(e){

           e.preventDefault();

           var csrf_token  = "{{ csrf_token() }}";
           var id =$(this).attr('data-consultant_id');
           var whichtr = $(this).closest("tr");

           swal({
               title: "Are you sure ?",
               text: "You want to delete this consultant ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
           .then((willDelete) => {
               if (willDelete) {

                   $.ajax({
                       type: "GET",
                       url: "{{route('consultant.destroy')}}",
                       data: {id:id,_token:csrf_token},
                       success: function(result) {
                           if(result.type == 'success'){
                               whichtr.remove();
                               swal('consultant successfully deleted','success');
                           }else{
                               swal('Whoa!','Something Went Wrong.','error');
                           }
                       }
                   });

               } else {
                   swal("Your consultant is safe!");
               }
           });

       });

       $('.consultant_status').on('click',function(e){
           e.preventDefault();
           var csrf_token  = "{{ csrf_token() }}";
           var id = $(this).attr('data-consultant_id');
           var status = $(this).attr('data-consultant_status');

           swal({
               title: "Are you sure ?",
               text: "Are you sure you want to change status ?",
               icon: "warning",
               buttons: true,
               dangerMode: true,
           })
               .then((willDelete) => {
                   if (willDelete) {

                       $.ajax({
                           type: "GET",
                           url: "{{route('consultant.status')}}",
                           data: {id:id,status:status,_token:csrf_token},
                           success: function(result) {
                               if(result.type == 'success'){
                                   location.reload();
                               }
                           }
                       });

                   } else {
                       swal("Your status remian unchange !");
                   }
               });

       });


    </script>
@endsection