@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Consultant</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="editConsultantForm" action="{{route('consultant.update')}}" method="post" enctype="multipart/form-data">
                    {{method_field('PUT')}}
                        {{csrf_field()}}
                        <div class="box-body">

                            <div class="form-group">
                                <input type="hidden" name="consultant_id" value="{{$consultant_data->id}}">
                                <label for="name">Consultant Firstname</label>
                                <input type="text" class="form-control" name="firstname" placeholder="Enter Consultant Firstname.." value="{{$consultant_data->firstname}}">
                                @if ($errors->has('firstname'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Consultant Lastname</label>
                                <input type="text" class="form-control" name="lastname" placeholder="Enter Consultant Lastname.." value="{{$consultant_data->lastname}}">
                                @if ($errors->has('lastname'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Phone Number</label>
                                <input type="number" class="form-control" name="phone" placeholder="Enter Consultant Phone No.." value="{{$consultant_data->phone}}">
                                @if ($errors->has('phone'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Enter Consultant Email.." value="{{$consultant_data->email}}">
                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Subject Expert</label>
                                <input type="text" class="form-control" name="subject_expert" placeholder="Enter Consultant Field Expert.." value="{{$consultant_data->subject_expert}}">
                                @if ($errors->has('subject_expert'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('subject_expert') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{url('admin/consultant')}}" class="btn btn-success">Cancel</a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')

<script type="text/javascript">

$(function() {

    $('#editConsultantForm').validate({
        errorClass: 'text-danger',
        rules: {
            firstname: "required",
            phone:"required",
            subject_expert:"required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            firstname: "Please enter consultant firstname name",
            phone: "Please enter consultant phone number",
            email: "Please enter valid consultant email address",
            subject_expert: "Please enter consultant subject expert"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});

</script>

@stop