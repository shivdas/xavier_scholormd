<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('fonts/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">

  <link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">

  <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.css')}}">

  <link rel="stylesheet" href="{{asset('css/jasny_bootstrap/jasny-bootstrap.min.css')}}" />

</head>
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 @include('partials.header')
 @include('partials.sidebar')

  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @yield('content')
        <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('partials.footer')

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src="{{asset('plugins/jQuery/jquery.validate.js')}}"></script>
<script src="{{asset('plugins/jQuery/additional-methods.js')}}"></script>

<!-- Bootstrap 3.3.6 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/app.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>

<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>

<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script src="{{asset('plugins/datatables/dataTables.responsive.js')}}"></script>

<script src="{{asset('js/moment.min.js')}}"></script>

<script src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>

<script src="{{asset('js/jasny_bootstrap/jasny-bootstrap.min.js')}}"></script>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAYf6GqXu0iKu3GZg6JQJMk5ifN4lyWdRo"></script>

<script src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>

@yield('js')
</body>
</html>
