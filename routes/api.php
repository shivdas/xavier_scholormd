<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$api = app('Dingo\Api\Routing\Router');

// Auth token does not required here
$api->version('v1', ['namespace'=>'App\Http\Controllers'], function ($api) {
    $api->post('login','Api\AuthenticateController@authenticate');
    $api->post('register','Api\AuthenticateController@register');
    $api->post('logout','Api\AuthenticateController@logout');
    $api->get('verify/{token}','Api\AuthenticateController@verify');
    $api->post('forgot_password', 'Api\AuthenticateController@forgot_password');
    $api->post('change_forgot_password', 'Api\AuthenticateController@change_forgot_password');
});

// Auth token required here
$api->version('v1', ['middleware' => 'api.auth', 'namespace'=>'App\Http\Controllers'], function ($api) {

    $api->post('change_password', 'Api\AuthenticateController@changePassword');
    $api->get('getFact','Admin\FactController@getFact');
    $api->get('get_all_categories','Api\ApiController@getAllCategories');
    $api->get('get_sub_categories/{category_id}','Api\ApiController@getSubCategories');
    $api->get('slug/{category}','Api\ApiController@getCategory');
    $api->post('save_consult_expert','Api\ApiController@save_consult_expert');
    $api->get('get_consult_expert','Api\ApiController@get_consult_expert');
    $api->get('get_consultant','Api\ApiController@get_consultant');
    $api->post('get_chapters','Api\ApiController@get_chapters');
    $api->post('get_chapter_description','Api\ApiController@get_chapter_description');
    $api->get('get_medical_posts','Api\ApiController@get_medical_posts');
    $api->get('my_profile','Api\ApiController@my_profile');
    $api->post('update_profile','Api\ApiController@update_profile');

    $api->get('get_highlight','Api\ApiController@get_highlight');
    $api->post('save_highlight','Api\ApiController@save_highlight');
    $api->get('get_notes','Api\ApiController@get_notes');
    $api->post('save_notes','Api\ApiController@save_notes');

});

