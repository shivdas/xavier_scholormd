<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Forgot Password*/
Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/reset','Auth\ResetPasswordController@reset');
Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::get('/',['as'=>'admin.login','uses'=>'Admin\AuthController@showLoginForm']);
Route::group(['prefix'=>'admin','namespace'=>'Admin'],function(){

		Route::get('/',function(){
			return view('errors.404');
		});
		
		Route::post('/',['uses'=>'AuthController@login']);
		Route::get('/forgotpassword', function () {
    		return view('auth.forgotPassword');
		})->name('forgot-password');

	Route::group(['middleware'=>['checkAdmin:admin']],function (){

		Route::get('/dashboard',['as'=>'admin.dashboard','uses'=>'DashboardController@index']);

		/*Fact Module*/
		Route::resource('facts','FactController');
		Route::post('all-facts',['as'=>'facts.allFacts','uses'=>'FactController@allFacts']);
		Route::get('facts-destroy/{id}','FactController@destroy')->name('facts.delete');
		Route::get('facts-status','FactController@activeStatus')->name('facts.status');

        /* Post Moduele */
        Route::post('posts/store','PostController@store')->name('posts.store');

        /*Places Module*/
        Route::get('places/{id?}','PlaceController@index')->name('places.allPlaces');
        Route::get('places/create/{id}','PlaceController@create')->name('places.create');
        Route::post('places/store','PlaceController@store')->name('places.store');
        Route::put('places/{id}','PlaceController@update')->name('places.update');
        Route::get('places/{id}/edit','PlaceController@edit')->name('places.edit');
        Route::get('places/destroy/{id}','PlaceController@destroy')->name('places.destroy');

        /*Cms Module*/
        Route::resource('cms', 'CmsController');
        Route::get('cms/updatestatus/{id}','CmsController@updatestatus');
        Route::post('cms/delete','CmsController@delMulticms');








		/*Category Module*/
		Route::get('category','CategoryController@index')->name('category.index');
		Route::get('category/create','CategoryController@create')->name('category.create');
		Route::post('category/store','CategoryController@store')->name('category.store');
		Route::get('category/{id}/edit','CategoryController@edit')->name('category.edit');
		Route::put('category','CategoryController@update')->name('category.update');
		Route::get('category/destroy','CategoryController@destroy')->name('category.destroy');
        Route::get('category-status','CategoryController@activeStatus')->name('category.status');

        /*SubCategory Module*/
        Route::get('subcat','SubcatController@index')->name('subcat.index');
        Route::get('subcat/create','SubcatController@create')->name('subcat.create');
        Route::post('subcat/store','SubcatController@store')->name('subcat.store');
        Route::get('subcat/{id}/edit','SubcatController@edit')->name('subcat.edit');
        Route::put('subcat','SubcatController@update')->name('subcat.update');
        Route::get('subcat/destroy','SubcatController@destroy')->name('subcat.destroy');
        Route::get('subcat-status','SubcatController@activeStatus')->name('subcat.status');

        /*Chapter Module*/
        Route::get('chapter','ChapterController@index')->name('chapter.index');
        Route::get('chapter/create','ChapterController@create')->name('chapter.create');
        Route::post('chapter/store','ChapterController@store')->name('chapter.store');
        Route::get('chapter/{id}/edit','ChapterController@edit')->name('chapter.edit');
        Route::put('chapter','ChapterController@update')->name('chapter.update');
        Route::get('chapter/destroy','ChapterController@destroy')->name('chapter.destroy');
        Route::get('chapter-status','ChapterController@activeStatus')->name('chapter.status');
        Route::get('subCat-list','ChapterController@subCatList')->name('chapter.subCatList');

        /*Consultant Module*/
        Route::get('consultant','ConsultantController@index')->name('consultant.index');
        Route::get('consultant/create','ConsultantController@create')->name('consultant.create');
        Route::post('consultant/store','ConsultantController@store')->name('consultant.store');
        Route::get('consultant/{id}/edit','ConsultantController@edit')->name('consultant.edit');
        Route::put('consultant','ConsultantController@update')->name('consultant.update');
        Route::get('consultant/destroy','ConsultantController@destroy')->name('consultant.destroy');
        Route::get('consultant-status','ConsultantController@activeStatus')->name('consultant.status');

        /*Medical Post Module*/
        Route::get('medical','MedicalController@index')->name('medical.index');
        Route::get('medical/create','MedicalController@create')->name('medical.create');
        Route::post('medical/store','MedicalController@store')->name('medical.store');
        Route::get('medical/{id}/edit','MedicalController@edit')->name('medical.edit');
        Route::put('medical','MedicalController@update')->name('medical.update');
        Route::get('medical/destroy','MedicalController@destroy')->name('medical.destroy');
        Route::get('medical-status','MedicalController@activeStatus')->name('medical.status');

        /*Consultant User*/
        Route::get('user','UserController@index')->name('user.index');
        Route::get('user/create','UserController@create')->name('user.create');
        Route::post('user/store','UserController@store')->name('user.store');
        Route::get('user/{id}/edit','UserController@edit')->name('user.edit');
        Route::get('user/{id}/show_notes','UserController@show_notes')->name('user.show_notes');
        Route::put('user','UserController@update')->name('user.update');
        Route::get('user/destroy','UserController@destroy')->name('user.destroy');
        Route::get('user-status','UserController@activeStatus')->name('user.status');

        /*Enquirie Module*/
        Route::get('enquirie','EnquirieController@index')->name('enquirie.index');
        Route::get('enquirie/destroy','EnquirieController@destroy')->name('enquirie.destroy');
        Route::post('assigne-consultant','EnquirieController@assigne_consultant')->name('enquirie.assignConsultant');


        /*User Selection Module*/
        Route::get('selection','UserSelectionController@index')->name('selection.index');


	});
		Route::get('/logout',['as'=>'logout','uses'=>'AuthController@logout']);
});

    Route::get('storage/images/medical_post/{filename}', function ($filename)

    {
        $path = storage_path('app/public/images/medical_post/' . $filename);

        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    });

    Route::get('storage/images/profile_pic/{filename}', function ($filename)
    {
        $path = storage_path('app/public/images/profile_pic/' . $filename);

        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;

    });