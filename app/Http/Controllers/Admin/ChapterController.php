<?php

namespace App\Http\Controllers\Admin;

use App\Subcat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Chapter;
use App\Place;
use App\Cms;
use Yajra\Datatables\Datatables;
use Validator;
class ChapterController extends Controller
{
  
    public function index(){

        $allChapter = array();
        $allChapter = \DB::select('
                                    SELECT id,
                                          (SELECT cat_name
                                           FROM categories
                                           WHERE id=cat_id) AS cat_name,
                                          (SELECT subcat_name
                                           FROM sub_categories
                                           WHERE id=subcat_id) AS subcat_name,sub_title,is_active,created_at,
                                           chapter_name,section_data
                                           description
                                    FROM chapters
                                    WHERE deleted_at IS NULL
                                    ORDER BY chapters.id DESC;
                                ');
    	return view('admin.chapter.index',compact('allChapter'));
    }

    public function create(){

        $allcategory = array();
        $allcategory = Category::select('id','cat_name')->where('is_active','=',1)->whereNull('deleted_at')->get();
        $allsubcategory = Subcat::select('id','subcat_name')->where('is_active','=',1)->whereNull('deleted_at')->get();
    	return view('admin.chapter.create',compact('allcategory','allsubcategory'));
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[

            'cat_id' => 'required',
            'subcat_id' => 'required',
            'chapter_name' => 'required'

        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }   
      try{

          //store section data :START
          $section_title_arr = $description_arr = $description_data = $section_data = array();

          foreach ($request->all() as $key => $value){

              if(strpos($key,'section_title_')!==false){//get all section title
                  $section_title_arr[] = $value;
              }
              if(strpos($key,'description_')!==false){//get all section description
                  $description_arr[] = $value;
              }
          }
          if(!empty($section_title_arr)){

              foreach ($section_title_arr as $k => $val){
                  if(isset($description_arr[$k]) && $description_arr[$k] != ''){
                      $section_data[$k]['section_title'] = !empty($section_title_arr[$k]) ? $section_title_arr[$k] : '';//data for section  column
                      $section_data[$k]['description'] = !empty($description_arr[$k]) ? $description_arr[$k] : '';//data for section column
                  }

              }

          }
          //store section data :END
          $chapter = new Chapter();
          $chapter->cat_id = $request->cat_id;
          $chapter->subcat_id = $request->subcat_id;
          $chapter->chapter_name = $request->chapter_name;
          $chapter->sub_title = $request->sub_title;
          $chapter->description = !empty($request->chapter_description) ? $request->chapter_description : '';
          $chapter->section_data = count($section_data)>0 ? json_encode($section_data) : '';
          $chapter->slug = 'chapter_name';

          $chapter->save();
            return redirect()->route('chapter.index')->with(['message'=>'Chapter details was successfully added','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('chapter.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function edit($id){

    	try {

            $allcategory = $subcategory = $desc_section = $sections = array();
            $allcategory = Category::select('id','cat_name')->where('is_active','=',1)->whereNull('deleted_at')->get();
            $allsubcategory = Subcat::select('id','subcat_name')->where('is_active','=',1)->whereNull('deleted_at')->get();
            $chapter_data = Chapter::findOrFail($id);
            $desc_section = $chapter_data->description;
            $sections = json_decode($chapter_data->section_data);
            return view('admin.chapter.edit', compact('chapter_data','allcategory','allsubcategory','desc_section','sections'));

        }catch (\Exception $e){
            return redirect()->route('chapter.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

     public function update(Request $request){

         $validator = Validator::make($request->all(),[

             'cat_id' => 'required',
             'subcat_id' => 'required',
             'chapter_name' => 'required'

         ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }  

        try{

            //store section data :START

            $section_title_arr = $description_arr = $description_data = $section_data = array();

            foreach ($request->all() as $key => $value){

                if(strpos($key,'section_title_')!==false){//get all section title
                    $section_title_arr[] = $value;
                }
                if(strpos($key,'description_')!==false){//get all section description
                    $description_arr[] = $value;
                }
            }
            if(count($section_title_arr) > 0){

                foreach ($section_title_arr as $k => $val){
                    $section_data[$k]['section_title'] = !empty($section_title_arr[$k]) ? $section_title_arr[$k] : '';//data for section column
                    $section_data[$k]['description'] = !empty($description_arr[$k]) ? $description_arr[$k] : '';//data for section column
                }

            }
            //store section data :END
            $chapter = Chapter::findOrFail($request->chapter_id);
            $chapter->cat_id = $request->cat_id;
            $chapter->subcat_id = $request->subcat_id;
            $chapter->chapter_name = $request->chapter_name;
            $chapter->sub_title = $request->sub_title;
            $chapter->description = $request->chapter_description;
            $chapter->section_data = count($section_data)>0 ? json_encode($section_data) : '';
            $chapter->save();
            return redirect()->route('chapter.index')->with(['message'=>'Chapter details was successfully updated','class'=>'alert-success']);

        }catch (\Exception $e){

            return redirect()->route('chapter.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function destroy(Request $request){

        try {

            Chapter::destroy($request->id);
            return response()->json(['type' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['type' => 'error']);
        }
    }

    public function activeStatus(Request $request){

        try{

            if(isset($request->id) && !empty($request->id)){
                $id = $request->id;
                $status = $request->status;

                if($status == 1){
                    $status = 0;
                }else{
                    $status = 1;
                }
                $category = Chapter::findOrFail($id);
                $category->is_active = $status;
                $category->save();
                return response()->json(['type' => 'success']);
            }
        }catch(\Exception $e){
            return response()->json(['type' => 'error']);
        }
    }

    public function subCatList(Request $request){

        $subCatList = array();
        if(!empty($request->category_id)){
            $subCatList = Subcat::select('id','subcat_name')->where('cat_id','=',$request->category_id)->where('is_active','=',1)->whereNull('deleted_at')->get();
        }
        return $subCatList;

    }


}
