<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Consultant;
use App\Enquirie;
use Yajra\Datatables\Datatables;
use Validator;
use App\Mail\NotifyConsultant;


class EnquirieController extends Controller
{
    public function index(){

        $allEnquirie = array();
        $allEnquirie = \DB::select('
                                    SELECT *,
                                      (SELECT CONCAT(firstname, \' \', lastname)
                                       FROM consultant
                                       WHERE id = consultant_id) AS expert_name,CONCAT(firstname, \' \', lastname) as user_name
                                    FROM enquiries
                                    WHERE deleted_at IS NULL
                                    ORDER BY id DESC;
                                ');
        $allConsutants = Consultant::where('is_active','=',1)->whereNull('deleted_at')->orderBy('id','desc')->get();

    	return view('admin.enquirie.index',compact('allEnquirie','allConsutants'));
    }

    public function assigne_consultant(Request $request){

        $validator = Validator::make($request->all(),[

            'consultant_id' => 'required'

        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }
        try{
            $prev_consutant_id = Enquirie::find($request->enq_id)->consultant_id;

            if($prev_consutant_id != $request->consultant_id){
                $enquirie = new Enquirie();
                $enquirie = $enquirie->findOrFail($request->enq_id);
                $enquirie->consultant_id = $request->consultant_id;
                $enquirie->consultant_assigne_on = date('Y-m-d H:i:s');
                $enquirie->save();
                $this->notify_to_consultant($enquirie->consultant_id);
                return redirect()->route('enquirie.index')->with(['message'=>'Consultant details was successfully added','class'=>'alert-success']);

            }else{
                return redirect()->route('enquirie.index')->with(['message'=>'Already updated','class'=>'alert-success']);
            }


        }catch (\Exception $e){
            return redirect()->route('enquirie.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }

    }

    public function destroy(Request $request){

        try {
            Enquirie::destroy($request->id);
            return response()->json(['type' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['type' => 'error']);
        }
    }

    public function notify_to_consultant($consultant_id){

        try {

            $consutant_email_id = Consultant::find($consultant_id)->email;
            $email = new NotifyConsultant();
            \Mail::to($consutant_email_id)->send($email);

        } catch (\Exception $e) {

        }



    }


}
