<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Subcat;
use App\Chapter;
use App\Consultant;
use App\Fact;
class DashboardController extends Controller
{
    public function index(){
    	$data['category'] = count(Category::all());
    	$data['subcat'] = count(Subcat::all());
    	$data['chapter'] = count(Chapter::all());
    	$data['consultant'] = count(Consultant::all());
    	$data['fact'] = count(Fact::all());
        return view('admin.dashboard',compact('data'));
    }
}
