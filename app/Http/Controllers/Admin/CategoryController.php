<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Place;
use App\Cms;
use Yajra\Datatables\Datatables;
use Validator;
class CategoryController extends Controller
{
    public function index(){

        $allCategory = Category::whereNull('deleted_at')->orderBy('id','desc')->get();


    	return view('admin.category.index',compact('allCategory'));
    }

    public function create(){
        
    	return view('admin.category.create');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[

            'cat_name' => 'required|unique:categories',
            'cat_name.required'=>'This field is required'

        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }   
      try{
            $category = new Category();
            $category->cat_name = $request->cat_name;
            $category->sub_title = $request->sub_title;
            $category->slug = 'category_type';
            $category->save();
            return redirect()->route('category.index')->with(['message'=>'Category details was successfully added','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('category.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function edit($id){

    	try {
            $category = Category::findOrFail($id);
            return view('admin.category.edit', compact('category'));

        }catch (\Exception $e){
            return redirect()->route('category.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

     public function update(Request $request){

         $validator = Validator::make($request->all(),[

             'cat_name' => 'required|unique:categories,cat_name,'.$request->cat_id,
             'cat_name.required'=>'This field is required',
             'cat_name.unique'=>'The category name has already been taken.',

         ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }  

        try{
            $category = new Category();
            $category = $category->findOrFail($request->cat_id);
            $category->cat_name = $request->cat_name;
            $category->sub_title = $request->sub_title;
            $category->save();
            return redirect()->route('category.index')->with(['message'=>'Category details was successfully updated','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('category.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function destroy(Request $request){

        try {
            Category::destroy($request->id);
            return response()->json(['type' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['type' => 'error']);
        }
    }

    public function activeStatus(Request $request){
        try{

            if(isset($request->id) && !empty($request->id)){
                $id = $request->id;
                $status = $request->status;

                if($status == 1){
                    $status = 0;
                }else{
                    $status = 1;
                }
                $category = Category::findOrFail($id);
                $category->is_active = $status;
                $category->save();
                return response()->json(['type' => 'success']);
//                return redirect()->route('category.index')->with(['message'=>'Category status were successfully changed','class'=>'alert-success']);
            }
        }catch(\Exception $e){
            return response()->json(['type' => 'error']);
//            return redirect()->route('category.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

    /*
        Get the category and places data according to slug.
        If category data not found then it will check the Cms data.
    */
    public function getCategory($slug){
        try{
            /*Checking category data*/
            $data = Category::with('places')->where('slug',$slug)->first();
            if(isset($data) && !empty($data)){
                return response()->json(['status'=>1,'message'=>'Fetched Data successfully','data'=>$data],200);
            }else{
                /*Checking Cms data*/
                $data = Cms::where('slug',$slug)->where('active',1)->first();
                if(isset($data) && !empty($data)){
                    return response()->json(['status'=>1,'message'=>'Fetched Data successfully','data'=>$data],200);
                }else{
                    return response()->json(['status'=>0,'message'=>'No Data Found'],200);
                }
            }
        }catch(\Exception $e){
            return response()->json(['status'=>0,'message'=>'Something Went wrong'],500);
        }
    }

    public function getAllCategories(){
        try{
            
            $allCategory = Category::select('category_name','slug')->get();
            return response()->json(['status'=>1,'data'=>$allCategory],200);
        }catch(\Exception $e){
            
            return response()->json(['status'=>0,'message'=>'Something went wrong.Could not fetchd list.'],500);
        }
    }

}
