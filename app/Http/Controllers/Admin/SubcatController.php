<?php

namespace App\Http\Controllers\Admin;

use App\Subcat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Place;
use App\Cms;
use Yajra\Datatables\Datatables;
use Validator;
class SubcatController extends Controller
{
    public function index(){

        $allSubCategory = array();
        $allSubCategory = \DB::select('SELECT id,
                                          (SELECT cat_name
                                           FROM categories
                                           WHERE id = cat_id) AS cat_name,sub_title,is_active,created_at,
                                           subcat_name
                                        FROM sub_categories where sub_categories.deleted_at IS NULL
                                        ORDER BY id DESC');

    	return view('admin.subcat.index',compact('allSubCategory'));
    }

    public function create(){

        $allcategory = array();
        $allcategory = Category::select('id','cat_name')->where('is_active','=',1)->whereNull('deleted_at')->get();

    	return view('admin.subcat.create',compact('allcategory'));
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[

            'cat_id' => 'required',
            'subcat_name' => 'required'

        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }   
      try{
          $subcategory = new Subcat();
          $subcategory->cat_id = $request->cat_id;
          $subcategory->subcat_name = $request->subcat_name;
          $subcategory->sub_title = $request->sub_title;
          $subcategory->slug = 'subcat_name';
          $subcategory->save();
            return redirect()->route('subcat.index')->with(['message'=>'Subcategory details was successfully added','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('subcat.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function edit($id){

    	try {

            $allcategory = $subcategory = array();
            $allcategory = Category::select('id','cat_name','sub_title')->get();
            $subcategory = Subcat::findOrFail($id);

            return view('admin.subcat.edit', compact('subcategory','allcategory'));

        }catch (\Exception $e){
            return redirect()->route('subcat.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

     public function update(Request $request){

         $validator = Validator::make($request->all(),[

             'subcat_id' => 'required',
             'cat_id' => 'required',
             'subcat_name' => 'required',

         ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }  

        try{
            $subcategory = new Subcat();
            $subcategory = $subcategory->findOrFail($request->subcat_id);
            $subcategory->cat_id = $request->cat_id;
            $subcategory->subcat_name = $request->subcat_name;
            $subcategory->sub_title = $request->sub_title;
            $subcategory->save();

            return redirect()->route('subcat.index')->with(['message'=>'Subcategory details was successfully updated','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('subcat.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function destroy(Request $request){

        try {

            Subcat::destroy($request->id);
            return response()->json(['type' => 'success']);
        } catch (\Exception $e) {
        }
    }

    public function activeStatus(Request $request){

        try{

            if(isset($request->id) && !empty($request->id)){
                $id = $request->id;
                $status = $request->status;

                if($status == 1){
                    $status = 0;
                }else{
                    $status = 1;
                }
                $category = Subcat::findOrFail($id);
                $category->is_active = $status;
                $category->save();
                return response()->json(['type' => 'success']);
            }
        }catch(\Exception $e){
            return response()->json(['type' => 'error']);
        }
    }

    /*
        Get the category and places data according to slug.
        If category data not found then it will check the Cms data.
    */
    public function getCategory($slug){
        try{
            /*Checking category data*/
            $data = Category::with('places')->where('slug',$slug)->first();
            if(isset($data) && !empty($data)){
                return response()->json(['status'=>1,'message'=>'Fetched Data successfully','data'=>$data],200);
            }else{
                /*Checking Cms data*/
                $data = Cms::where('slug',$slug)->where('active',1)->first();
                if(isset($data) && !empty($data)){
                    return response()->json(['status'=>1,'message'=>'Fetched Data successfully','data'=>$data],200);
                }else{
                    return response()->json(['status'=>0,'message'=>'No Data Found'],200);
                }
            }
        }catch(\Exception $e){
            return response()->json(['status'=>0,'message'=>'Something Went wrong'],500);
        }
    }

    public function getAllCategories(){
        try{
            
            $allCategory = Subcat::select('category_name','slug')->get();
            return response()->json(['status'=>1,'data'=>$allCategory],200);
        }catch(\Exception $e){
            
            return response()->json(['status'=>0,'message'=>'Something went wrong.Could not fetchd list.'],500);
        }
    }
}
