<?php

namespace App\Http\Controllers\Admin;

use App\Subcat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Consultant;
use Yajra\Datatables\Datatables;
use Validator;
class ConsultantController extends Controller
{
    public function index(){

        $allConsultant = array();
        $allConsultant = Consultant::whereNull('deleted_at')->get();
    	return view('admin.consultant.index',compact('allConsultant'));
    }

    public function create(){

        return view('admin.consultant.create');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[

            'firstname' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'subject_expert' => 'required'

        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }
        try{

            $consultant = new Consultant();
            $consultant->firstname = $request->firstname;
            $consultant->lastname = $request->lastname;
            $consultant->phone = $request->phone;
            $consultant->email = $request->email;
            $consultant->subject_expert = $request->subject_expert;
            $consultant->slug = 'consultant_name';

            $consultant->save();
            return redirect()->route('consultant.index')->with(['message'=>'Consultant details was successfully added','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('consultant.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function edit($id){

        try {

            $consultant_data = Consultant::findOrFail($id);

            return view('admin.consultant.edit', compact('consultant_data'));

        }catch (\Exception $e){
            return redirect()->route('consultant.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[

            'firstname' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'subject_expert' => 'required'

        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }

        try{
            $consultant = Consultant::findOrFail($request->consultant_id);
            $consultant->firstname = $request->firstname;
            $consultant->lastname = $request->lastname;
            $consultant->phone = $request->phone;
            $consultant->email = $request->email;
            $consultant->subject_expert = $request->subject_expert;
            $consultant->save();
            return redirect()->route('consultant.index')->with(['message'=>'Consultant details was successfully updated','class'=>'alert-success']);

        }catch (\Exception $e){

            return redirect()->route('consultant.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function destroy(Request $request){

        try {

            Consultant::destroy($request->id);
            return response()->json(['type' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['type' => 'error']);
        }
    }

    public function activeStatus(Request $request){

        try{

            if(isset($request->id) && !empty($request->id)){
                $id = $request->id;
                $status = $request->status;

                if($status == 1){
                    $status = 0;
                }else{
                    $status = 1;
                }
                $category = Consultant::findOrFail($id);
                $category->is_active = $status;
                $category->save();
                return response()->json(['type' => 'success']);
            }
        }catch(\Exception $e){
            return response()->json(['type' => 'error']);
        }
    }




}
