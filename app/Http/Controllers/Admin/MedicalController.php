<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Medical;
use App\Place;
use App\Cms;
use Yajra\Datatables\Datatables;
use Validator;
class MedicalController extends Controller
{
    public function index(){

        $allMedical = Medical::whereNull('deleted_at')->orderBy('id','desc')->get();


    	return view('admin.medical.index',compact('allMedical'));
    }

    public function create(){
        
    	return view('admin.medical.create');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[

            'post_name' => 'required',
            'post_image' => 'image|mimes:jpg,png,jpeg|max:2048'

        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }   
      try{

          if($request->post_image)
          {
              $name = $request->post_image->getClientOriginalName();
              $extension = $request->post_image->getClientOriginalExtension();;
              $image_name = pathinfo($name, PATHINFO_FILENAME).'_'.time().'.'.$extension;
              $path = storage_path().'/app/public/images/medical_post';
              $request->post_image->move($path, $image_name);
          }

          $medical = new Medical();
          $medical->post_name = $request->post_name;
          $medical->post_image = !empty($image_name) ? $image_name : '';
          $medical->short_desc = $request->short_desc;
          $medical->lagre_desc = $request->lagre_desc;
          $medical->slug = 'medical_post';
          $medical->save();
            return redirect()->route('medical.index')->with(['message'=>'Medical details was successfully added','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('medical.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function edit($id){

    	try {
            $medical = Medical::findOrFail($id);
            return view('admin.medical.edit', compact('medical'));

        }catch (\Exception $e){
            return redirect()->route('medical.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

     public function update(Request $request){




         $validator = Validator::make($request->all(),[

             'post_name' => 'required',
             'post_image' => 'image|mimes:jpg,png,jpeg|max:2048'

         ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }  

        try{

            if($request->post_image)
            {
                $name = $request->post_image->getClientOriginalName();
                $extension = $request->post_image->getClientOriginalExtension();;
                $image_name = pathinfo($name, PATHINFO_FILENAME).'_'.time().'.'.$extension;
                $path = storage_path().'/app/public/images/medical_post';
                $request->post_image->move($path, $image_name);
            }

            $medical = new Medical();
            $medical = $medical->findOrFail($request->medical_id);
            $medical->post_name = $request->post_name;
            $medical->post_image = !empty($image_name) ? $image_name : '';
            $medical->short_desc = $request->short_desc;
            $medical->lagre_desc = $request->lagre_desc;
            $medical->save();
            return redirect()->route('medical.index')->with(['message'=>'Medical details was successfully updated','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('medical.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function destroy(Request $request){

        try {
            Medical::destroy($request->id);
            return response()->json(['type' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['type' => 'error']);
        }
    }

    public function activeStatus(Request $request){
        try{

            if(isset($request->id) && !empty($request->id)){
                $id = $request->id;
                $status = $request->status;

                if($status == 1){
                    $status = 0;
                }else{
                    $status = 1;
                }
                $medical = Medical::findOrFail($id);
                $medical->is_active = $status;
                $medical->save();
                return response()->json(['type' => 'success']);
            }
        }catch(\Exception $e){
            return response()->json(['type' => 'error']);
        }
    }

}
