<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Consultant;
use App\UserSelection;
use Yajra\Datatables\Datatables;
use Validator;

class UserSelectionController extends Controller
{
    public function index(){

        $allSelection = array();
        $allSelection = \DB::select('
                                    SELECT *,
                                      (SELECT CONCAT(firstname, \' \', lastname)
                                       FROM users
                                       WHERE id = user_id) AS user_name,
                                      (SELECT chapter_name
                                       FROM chapters
                                       WHERE id = chapter_id) AS chapter_name
                                    FROM user_selection
                                    WHERE deleted_at IS NULL
                                    ORDER BY id DESC;
                                ');

    	return view('admin.selection.index',compact('allSelection'));
    }






}
