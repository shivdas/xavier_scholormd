<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Fact;
use Yajra\Datatables\Datatables;
use Validator;
use Carbon\Carbon;
class FactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*Fetching all the facts*/
    public function index()
    {
        $allFacts = Fact::all();
        return view('admin.facts.index',compact('allFacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.facts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*Storing the fact details*/
    public function store(Request $request)
    {
        $data = $request->all();
        $validator=Validator::make($data, [
            'name' => 'required',
            'description'=>'required',
           // 'fact_date'=>'required|date'
         ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }   

      try{
            $fact = new Fact();
            $fact->name=$request->name;
            $fact->description=$request->description;

            /*if($request->fact_date){
                $fact->fact_date = Carbon::createFromFormat('m/d/Y',$request->fact_date)->format('Y-m-d');
            }*/
            $fact ->save();
            return redirect()->route('facts.index')->with(['message'=>'Fact details were successfully added','class'=>'alert-success']);
        }catch (\Exception $e){
            return redirect()->route('facts.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*showing the fact details*/
    public function edit($id)
    {
        try {
            $facts = Fact::findOrFail($id);
            return view('admin.facts.edit', compact('facts'));
        }catch (\Exception $e){
            return redirect()->route('facts.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*Update the fact details*/
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator=Validator::make($data, [
            'name' => 'required',
            'description'=>'required',
            //'fact_date'=>'required|date'
         ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }           
        try{
            $fact = new Fact();
            $fact = $fact->findOrFail($id);
            $fact->name = $request->name;
            $fact->description = $request->description;

            /*if($request->fact_date){
                $fact->fact_date = Carbon::createFromFormat('m/d/Y',$request->fact_date)->format('Y-m-d');
            }*/
            
            $fact->save(); 
            return redirect()->route('facts.index')->with(['message'=>'Fact details were successfully updated','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('facts.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /*destroy the fact*/
    public function destroy($id)
    {
        try {
            $fact = Fact::destroy($id);
           return redirect()->route('facts.index')->with(['message'=>'Fact details were successfully Deleted','class'=>'alert-success']);
        }catch(\Exception $e){
             return redirect()->route('facts.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

//changing the status of fact(Active or Inactive)
    public function activeStatus(Request $request){
        try{

            if(isset($request->id) && !empty($request->id)){
                $id = $request->id;
                $status = $request->status;

                if($status == 1){
                    $status = 0;
                }else{
                    $status = 1;
                }
                
                $fact = Fact::findOrFail($id);
                $fact->is_active = $status;
                $fact->save();
                return redirect()->route('facts.index')->with(['message'=>'Fact status were successfully changed','class'=>'alert-success']);
            }
        }catch(\Exception $e){
            return redirect()->route('facts.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

    //get current date fact if its not present then return random fact
    /*public function getFact(){
        try{
            $currentDate = Carbon::now()->format('Y-m-d');
            $allFacts = Fact::where('fact_date',$currentDate)->where('is_active',1)->get();
            if(isset($allFacts) && count($allFacts) >=1 ){

                return response()->json(['status'=>1,'message'=>'Fetched Data successfully','data'=>$allFacts],200);
            }else{
                $allFacts = Fact::orderByRaw("RAND()")->where('is_active',1)->first();
                return response()->json(['status'=>1,'message'=>'Fetched Data successfully','data'=>$allFacts],200);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>0,'message'=>'Something went wrong.Could not fetchd list.'],500);
        }
    }*/

    public function getFact(){
        try{
            $allFacts = Fact::orderByRaw("RAND()")->where('is_active',1)->first();
            return response()->json(['status'=>1,'message'=>'Fetched Data successfully','data'=>$allFacts],200);
        }catch(\Exception $e){
            return response()->json(['status'=>0,'message'=>'Something went wrong.Could not fetchd list.'],500);
        }
    }

}
