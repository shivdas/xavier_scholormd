<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Place;
use App\Category;
use Validator;
class PlaceController extends Controller
{
    public function index($id){
    	$places = Place::with('category')->where('category_id',$id)->get();
    	$categories = Category::findOrFail($id);
    	return view('admin.place.index',compact('places','categories'));
    }

    public function create($id){
    	$category_id = $id;
    	return view('admin.place.create',compact('category_id'));
    }

    public function store(Request $request){
    	$data = $request->all();
        $validator=Validator::make($data, [
            'name' => 'required',
            'contact_number' => 'required',
            'address' => 'required',
            'description' => 'required',
            'website' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
         ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }   
      try{
            $place = new Place();
                $place->name=$request->name;
                $place->website=$request->website; 
                $place->contact_number=$request->contact_number;
                $place->description=$request->description;
                $place->category_id = $request->category_id;
                if(isset($request->place_address) && !empty($request->place_address)){
            		$place->address = $request->place_address;
                    $place->lat = $request->lat;
                    $place->lng = $request->lng;
            	}
            $place ->save();
            return redirect()->route('places.allPlaces',['id'=>$place->category_id])->with(['message'=>'Place details were successfully added','class'=>'alert-success']);
        }catch (\Exception $e){
            return redirect()->route('places.allPlaces',['id'=>$place->category_id])->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function edit($id){
    	try {
            $place = Place::with('category')->findOrFail($id);
            return view('admin.place.edit', compact('place'));
        }catch (\Exception $e){
            return redirect()->route('places.allPlaces',['id'=>$place->category_id])->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

    public function update(Request $request,$id){
    	$data = $request->all();
        $validator=Validator::make($data, [
            'name' => 'required',
            'contact_number' => 'required',
            'address' => 'required',
            'description' => 'required',
            'website' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
         ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }           
        try{
            $place = new Place();
            $place = $place->findOrFail($id);
            $place->name = $request->name;
            $place->contact_number = $request->contact_number;
            $place->website = $request->website;
            $place->description = $request->description;
            $place->category_id = $request->category_id;
            if(isset($request->place_address) && !empty($request->place_address)){
                $place->address = $request->place_address;
                $place->lat = $request->lat;
            	$place->lng = $request->lng;
            }
            $place->save(); 
            return redirect()->route('places.allPlaces',['id'=>$place->category_id])->with(['message'=>'Place details were successfully updated','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('places.allPlaces',['id'=>$place->category_id])->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function destroy($id,Request $request){
    	try {
           $place = Place::destroy($id);
           return redirect()->route('places.allPlaces',['id'=>$request->category_id])->with(['message'=>'Place details were successfully Deleted','class'=>'alert-success']);
        }catch(\Exception $e){
             return redirect()->route('places.allPlaces',['id'=>$request->category_id])->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }
}
