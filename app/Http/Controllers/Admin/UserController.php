<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Notes;
use Yajra\Datatables\Datatables;
use Validator;

class UserController extends Controller
{
    public function index(){

        $allUser = array();
        $allUser = User::where('id','!=',1)->whereNull('deleted_at')->get();//remove admin from listing
    	return view('admin.user.index',compact('allUser'));
    }

    public function create(){

//        return view('admin.user.create');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[

            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'email' => 'required|email'

        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }
        try{

            $user = new User();
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->speciality = $request->speciality;
            $user->province = $request->province;
            $user->save();

            return redirect()->route('user.index')->with(['message'=>'Consultant details was successfully added','class'=>'alert-success']);

        }catch (\Exception $e){
            return redirect()->route('user.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function edit($id){

        try {

            $user_data = User::findOrFail($id);

            return view('admin.user.edit', compact('user_data'));

        }catch (\Exception $e){
            return redirect()->route('user.index')->with(['message'=>'Something went wrong','class'=>'alert-danger']);
        }
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(),[

            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'post_image' => 'image|mimes:jpg,png,jpeg|max:2048'

        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }

        try{

            if($request->profile_image)
            {
                $name = $request->profile_image->getClientOriginalName();
                $extension = $request->profile_image->getClientOriginalExtension();;
                $image_name = pathinfo($name, PATHINFO_FILENAME).'_'.time().'.'.$extension;
                $path = storage_path().'/app/public/images/profile_pic';
                $request->profile_image->move($path, $image_name);
            }
            $user = User::findOrFail($request->user_id);
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->speciality = $request->speciality;
            $user->province = $request->province;
            $user->profile_pic = !empty($image_name) ? $image_name : '';
            $user->save();

            return redirect()->route('user.index')->with(['message'=>'User details was successfully updated','class'=>'alert-success']);

        }catch (\Exception $e){

            return redirect()->route('user.index')->with(['message'=>'Something went wrong.Please try again!','class'=>'alert-danger']);
        }
    }

    public function destroy(Request $request){

        try {

            User::destroy($request->id);
            return response()->json(['type' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['type' => 'error']);
        }
    }

    public function activeStatus(Request $request){

        try{

            if(isset($request->id) && !empty($request->id)){
                $id = $request->id;
                $status = $request->status;

                if($status == 1){
                    $status = 0;
                }else{
                    $status = 1;
                }
                $user = User::findOrFail($id);
                $user->active = $status;
                $user->save();
                return response()->json(['type' => 'success']);
            }
        }catch(\Exception $e){
            return response()->json(['type' => 'error']);
        }
    }

    public function show_notes($id){

        $userNotes  = array();
        $userName = '';
        $userNotes = Notes::where('user_id','=',$id)->whereNull('deleted_at')->get();
        $userName = User::where('id','=',$id)->select('firstname', 'lastname')->first();

        return view('admin.user.show_notes',compact('userNotes','userName'));

    }




}
