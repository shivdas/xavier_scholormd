<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Fact;
use Carbon\Carbon;
class FactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facts=Fact::all();
        return response()->json(['status'=>1,'message'=>'Fetched facts list','data'=>$facts], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'     => 'required|unique:facts',
            'description'     => 'required',
            'fact_date'     => 'required|date'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>0,'message'=>'Validation failure','data'=>$validator->errors()], 200);
        }

       try{
            $fact = new Fact;
            $fact->name = $request->name;
            $fact->description = $request->description;
            $fact->fact_date = Carbon::parse($request->fact_date);
            $data = $fact;
            $fact->save();
            return response()->json(['status'=>1,'message'=>'created successfully','data'=>$data], 200);
        }catch(\Exception $e){
            return response()->json(['status'=>0,'error' => 'Could not created facts'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
        $fact = Fact::findOrFail($id);
        
        return response()->json(['status'=>1,'message'=>'Fetched facts','data'=>$fact], 200);
         }
         catch (\Exception $e) {
            // something went wrong while fetching facts
            return response()->json(['status'=>0,'error' => 'Could not fetch facts'], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validator = \Validator::make($request->all(), [
        'name'     => 'required|unique:facts,name,'.$id,
        'description' => 'required',
        'fact_date' =>'required|date'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>0,'message'=>'Validation failure','data'=>$validator->errors()], 200);
        }
       
        try
        {
            $fact = Fact::findOrFail($id);
            $fact->name = $request->name;
            $fact->description = $request->description;
            $fact->fact_date = Carbon::parse($request->fact_date);
            $data= $fact;
            $fact->save();
            return response()->json(['status'=>1,'message'=>'updated facts successfully','data'=>[ $data]], 200);
        }
        catch (\Exception $e) {
            // something went wrong while fetching facts
            return response()->json(['status'=>0,'error' => 'Could not update facts'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fact=Fact::destroy($id);
        if($fact)
        {
          return response()->json(['status'=>1,'message'=>'Fact deleted successfully','data'=>$fact], 200);
        }
        else
        {
            return response()->json(['status'=>0,'error' => 'unable to delete fact'], 500);
           
        }
    }

    public function status($id,$status){

        if($status == 1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $fact = Fact::find($id);
        $fact->is_active = $status;
        $fact->save();

        if($status){
            return response()->json(['status'=>1,'message'=>'Fact successfully activated','data'=>$fact], 200);
        }else{
            return response()->json(['status'=>1,'message'=>'Fact successfully deactivated','data'=>$fact], 200);
        }
    }
}
