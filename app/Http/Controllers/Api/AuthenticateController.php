<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use App\Category;
use App\Subcat;
use Illuminate\Auth\Events\Registered;
use App\Mail\EmailVerification;
use App\Mail\ResetPasswordapi;
use App\Mail\SendPassword;
use Illuminate\Support\Facades\Hash;
use Mail;
use DB;
use Session;

class AuthenticateController extends Controller
{
    /* public function login(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'email' => 'required|max:255',
            'password' => 'required|min:6',
            ]);

        if ($validator->fails()) {
            return response()->json(['status'=>0,'error'=>'Validation failure','error_logs'=>[$validator->errors()]], 200);

        }
        // grab credentials from the request
     	$credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['status'=>0,'error' => 'Invalid Credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong while attempting to encode the token
            return response()->json(['status'=>0,'error' => 'could not create token'], 500);
        }
        // if success returns the token
         return response()->json(['status'=>1,'message'=>'Authentication Success','data'=>compact('token')],200);
    } */



    public function authenticate(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'username' => 'required|max:255',
            'password' => 'required|min:6',
            ]);

        if ($validator->fails()) {

            $collection = collect($validator->errors());
            $plucked = $collection->pluck(0);

            return response()->json(['status'=>false,'error'=>$plucked], 200);

        }
        // grab credentials from the request

         if(filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
            $credentials = $request->only('username', 'password');
            $credentials['email']=$credentials['username'];
            unset($credentials['username']);
        }
        else {
          $credentials = $request->only('username', 'password');
        }
    
        try
        {   if(isset($credentials['email']))
            $user=User::where('email','=',$credentials['email'])->first();

            else
            $user=User::where('username','=',$credentials['username'])->first();

        }
        catch (\Exception $e) {
            // something went wrong while fetching user
            return response()->json([
                'status'=>false,
                'error' => ['Could not fetch user']
            ], 500);
        }

        try {

            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status'=>false,
                    'error' => ['Invalid Credentials']
                ], 200);
            }

            if(!$user->active)
            {
                return response()->json([
                    'status'=>false,
                    'error' => ['Account not activated ,kindly verify your email']
                ], 200);
            }
        

        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
                'status'=>false,
                'error' => ['could not create token']
            ], 200);
        }

        if($user->profile_pic){
            $user->profile_pic = asset('/images/user_images').'/'.$user->profile_pic;
        }
        if($user->cover_image){
            $user->cover_image = asset('/images/user_images').'/'.$user->cover_image;
        }

        $category = Category::select('id','cat_name','sub_title','slug')->with('subcategory')->get();
        // all good so return the token
         return response()->json([
             'status'=>true,
             'active_user' => true,
             'message'=>'Authentication Success',
             'data'=>compact('token','user','category'),
         ],200);
    }

    public function logout(Request $request) {
        // $this->validate($request, [
        //     'token' => 'required'
        //     ]);
        $token = JWTAuth::getToken();
        JWTAuth::invalidate($token);
        return response()->json(['status'=>true,'message'=>' logged out successfully ']);
    }

    protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
            'username' => $data['username'],
            'lastname' => $data['lastname'],
            'speciality' => $data['speciality'],
            'province' => $data['province'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'email_token' => str_random(30),
            'password' => bcrypt($data['password']),
            'active' => 0,
            ]);
    }

    public function register(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'username' => 'required|max:255|unique:users',
            'password' => 'required|min:6',
            ]);

        if ($validator->fails()) {


            $collection = collect($validator->errors());
            $plucked = $collection->pluck(0);

            return response()->json(['status'=>false,'message'=>'Validation error','error'=>$plucked], 200);
        }

        DB::beginTransaction();
        try
        {
            //$data = $request->all();
            $data['firstname'] = $request->firstname;
            $data['lastname'] = $request->lastname;
            $data['email'] = $request->email;
            $data['username'] = $request->username;
            $data['password'] = $request->password;
            $data['speciality'] = !empty($request->speciality) ? $request->speciality :'';
            $data['province'] = !empty($request->province) ? $request->province :'';
            $data['phone'] = !empty($request->phone) ? $request->phone :'';

            event(new Registered($user = $this->create($data)));

            $email = new EmailVerification(new User([
                      'email_token' => $user->email_token,
                      'firstname' => $user->firstname,
                      'email' => $user->email,
                      /*'password' => $password,*/
                    ]));
            $mail=Mail::to($user->email)->send($email);
            DB::commit();

            $token = JWTAuth::fromUser($user);

            return response()->json([
                'status'=>true,
                'message' => 'We have sent you an email with confirmation code',
                'token' => $token,
                'user'=>$user
            ], 200);

        }
        catch(QueryException $e)
        {
            DB::rollback();
            return response()->json(['status'=>false,'error' => ['Registration failed'],'details'=>$e], 500);
        }
    }

    public function verify($token)
    {
        // The verified method has been added to the user model and chained here
        // for better readability

        $user = User::where('email_token', $token)->first();
        if(!empty($user)){
            $user->verified();
            Session::flash('message', 'Success! Your account is verified. Please log in via the app.');
            echo "Success! Your account is verified. Please log in via the app."; exit;
        //return redirect('verifyAcc');     
        }else{
            echo "Link expired."; exit;
        }

    }

    public function resend_verification_code(){


        $email_token = rand(100000,999999);
        $user = JWTAuth::parseToken()->authenticate();

        $user = User::find($user->id);
        $user->email_token = $email_token;
        $user->save();

        $email = new EmailVerification(new User([
            'email_token' => $user->email_token,
            'firstname' => $user->firstname,
            'email' => $user->email,
            /*'password' => $password,*/
        ]));

        Mail::to($user->email)->send($email);
        return response()->json(['status'=>true,'message' => 'We have sent you an email with confirmation code'], 200);
    }

    public function changePassword(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'current_password' => 'required|min:6',
            'password' => 'required|min:6',
            ]);

        if ($validator->fails()) {

            $collection = collect($validator->errors());
            $plucked = $collection->pluck(0);
            return response()->json(['status'=>false,'error'=>$plucked], 200);

        }

        $data=$request->all();
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['status'=>false,'error'=>['user_not_found']], 200);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['status'=>false,'error'=>'token_expired'], $e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['status'=>false,'error'=>'token_invalid'], $e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['status'=>false,'error'=>'token_absent'], $e->getStatusCode());
        }
        $user=User::find($user->id);
        $old_password=$user->makeVisible('password')->password;
        if (Hash::check($data['current_password'], $old_password)) {

            $user->password = Hash::make($data['password']);
            $user->save();

            return response()->json(['status'=>true,"message"=>'Password changed successfully ']);
        }
        else
        {
            return response()->json(['status'=>false,"error"=>['Entered Current password did not match your password']]);
        }

    }

    public function forgot_password(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'error'=>[$validator->errors()]], 200);

        }
        
        try{
         $user=User::where('email','=',$request->email)->first();
         
         if($user->active == 0){
            return response()->json(['status'=>false,'error'=>["Account not verified."]], 200);
            exit;
         }
         
         $change_token=str_random(8);
         $user->email_token=$change_token;
         $status=$user->save();
         $email = new ResetPasswordapi(new User(['email_token' => $user->email_token]));
         $mail=Mail::to($user->email)->send($email);

          if($status)
          {
            return response()->json(['status'=>true,"message"=>'Reset code has been sent to your email address']);
          }
       }
        catch(\Exception $e) 
        {
         return response()->json(['status'=>false,"error"=>['Can not find any user account associated with entered email']]);

        }
    }
    
    public function change_forgot_password(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'code' => 'required|size:8',
            'password' => 'required|min:6'
            ]);

        if ($validator->fails()) {

            $collection = collect($validator->errors());
            $plucked = $collection->pluck(0);
            return response()->json(['status'=>false,'error'=>$plucked], 200);

        }
        try{
            $user=User::where('email_token','=',$request->code)->first();
            $user->password=bcrypt($request->password);
            $user->email_token=null;
            $status=$user->save();
            if($status)
            {
                return response()->json(['status'=>true,"message"=>'Password Reset Successfully']);
            }
        }
        catch(\Exception $e)
        {
            return response()->json(['status'=>false,"error"=>['Invalid code']]);
        }
    }
}
