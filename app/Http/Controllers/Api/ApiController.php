<?php

namespace App\Http\Controllers\Api;

use App\Highlight;
use App\Medical;
use App\Notes;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Enquirie;
use App\Chapters;
use App\Category;
use App\Subcat;
use App\Consultant;
use Validator;
use Auth;

class ApiController extends Controller
{
    public function save_consult_expert(Request $request){

        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'description' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {

            $collection = collect($validator->errors());
            $plucked = $collection->pluck(0);
            return response()->json(['status'=>false,'error'=>$plucked], 200);
        }

        $tbl = new Enquirie();

        $file = $request->file('doc') ? $request->file('doc') : '';
        if($file){
            $image = 'doc_'.time().'.'.$file->getClientOriginalExtension();
            $response = $file->move(public_path('/images/user_images/'), $image);
            $tbl->doc = $image;
        }

        $tbl->firstname = $request->firstname;
        $tbl->lastname = $request->lastname;
        $tbl->phone = $request->phone;
        $tbl->email = $request->email;
        $tbl->description = $request->description;
        /*$tbl->consultant_id = $request->consultant_id;*/
        if($tbl->save()){
            return response()->json(['status'=>true,'message'=>'Successfully inserted.']);
        }else{
            return response()->json(['status'=>false,'error'=>['Something went wrong']]);
        }
    }

    public function get_consult_expert(){

        $data = Enquirie::with('consultant')->get()->map(function ($q){
            if($q->doc){
                $q->doc = asset('/images/user_images').'/'.$q->doc;
            }
            return $q;
        });
        if(count($data) > 0){
            return response()->json(['stauts' => true,'message' => 'Success','data' => $data]);
        }else{
            return response()->json(['status' => true,'message' => 'Could not found any data', 'data' => []]);
        }
    }

    public function get_consultant(){

        $data = Consultant::get();
        if(count($data) > 0){
            return response()->json(['stauts' => true,'message' => 'Success','data' => $data]);
        }else{
            return response()->json(['status' => true,'message' => 'Could not found any data', 'data' => []]);
        }
    }

    public function getCategory($slug){
        try{
            /*Checking category data*/
            $data = Category::with('places')->where('slug',$slug)->first();
            if(isset($data) && !empty($data)){
                return response()->json(['status'=>true,'message'=>'Fetched Data successfully','data'=>$data],200);
            }else{
                /*Checking Cms data*/
                $data = Cms::where('slug',$slug)->where('active',1)->first();
                if(isset($data) && !empty($data)){
                    return response()->json(['status'=>true,'message'=>'Fetched Data successfully','data'=>$data],200);
                }else{
                    return response()->json(['status' => true,'message' => 'Could not found any data', 'data' => []]);
                }
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false,'error'=>['Something Went wrong']]);
        }
    }

    public function getAllCategories(){
        try{
            $allCategory = Category::get();
            return response()->json(['status'=>true,'data'=>$allCategory],200);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'error'=>['Something went wrong.Could not fetchd list.']]);
        }
    }

    public function getSubCategories($category_id){
        try{
            $data = Subcat::where('cat_id',$category_id)->get();
            return response()->json(['status'=>true,'data'=>$data],200);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'error'=>['Something went wrong.Could not fetchd list.']]);
        }
    }

    public function get_chapters(Request $request){
        $data = Chapters::select('id','chapter_name','sub_title','slug','cat_id','subcat_id')->with(['category','subCategory'])->where('subcat_id', $request->sub_category_id)->whereNull('deleted_at')->get();
        if(count($data) > 0){
            return response()->json(['status'=>true, 'message'=>'success', 'data'=>$data]);
        }else{
            return response()->json(['status'=>true, 'message'=>'No data found', 'data'=>[]]);
        }
    }

    public function get_chapter_description(Request $request){
        $data = Chapters::with(['category','subCategory'])->where('id', $request->chapter_id)->first();

        if($data){

            $data->description = json_decode($data->description);
            $data->section_data = json_decode($data->section_data);

            return response()->json(['status'=>true, 'message'=>'success', 'data'=>$data]);
        }else{
            return response()->json(['status'=>false, 'error'=>['Invaild chapter_id']]);
        }
    }

    public function get_medical_posts(Request $request){
        $data = Medical::get()->map(function($q){
            if($q->post_image){
                $q->post_image = asset("/images/medical_post").'/'.$q->post_image;
            }
            return $q;
        });
        if($data){
            return response()->json(['status'=>true, 'message'=>'success', 'data'=>$data]);
        }else{
            return response()->json(['status'=>true, 'message'=>'No data found', 'data'=>[]]);
        }
    }

    public function my_profile(){

        $data = User::find(Auth::id());
        if($data){

            if($data->profile_pic){
                $data->profile_pic = asset('/images/user_images').'/'.$data->profile_pic;
            }
            if($data->cover_image){
                $data->cover_image = asset('/images/user_images').'/'.$data->cover_image;
            }

            return response()->json(['status'=>true, 'message'=>'success', 'data'=>$data]);
        }else{
            return response()->json(['status'=>true, 'message'=>'No data found', 'data'=>[]]);
        }
    }

    public function update_profile(Request $request){

        $validator = \Validator::make($request->all(), [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $collection = collect($validator->errors());
            $plucked = $collection->pluck(0);
            return response()->json(['status'=>false,'error'=>$plucked], 200);
        }

        $user = User::find(Auth::id());

        $file = $request->file('profile_pic') ? $request->file('profile_pic') : '';
        if($file){
            $image = 'profile_'.time().'.'.$file->getClientOriginalExtension();
            $response = $file->move(public_path('/images/user_images/'), $image);
            $user->profile_pic = $image;
        }

        $file = $request->file('cover_image') ? $request->file('cover_image') : '';
        if($file){
            $image = 'cover_'.time().'.'.$file->getClientOriginalExtension();
            $response = $file->move(public_path('/images/user_images/'), $image);
            $user->cover_image = $image;
        }


        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->speciality = $request->speciality;
        $user->province = $request->province;
        $user->phone = $request->phone;
        if($user->save()){

            $data = User::find(Auth::id());
            if($data->profile_pic){
                $data->profile_pic = asset('/images/user_images').'/'.$data->profile_pic;
            }
            if($data->cover_image){
                $data->cover_image = asset('/images/user_images').'/'.$data->cover_image;
            }

            return response()->json(['status'=>true, 'message'=>'successfully updated', 'data'=>$data]);
        }else{
            return response()->json(['status'=>false, 'error'=>['Error']]);
        }
    }

    public function get_highlight(){
        $data = Highlight::where('user_id',Auth::id())->get();
        if($data){
            return response()->json(['status'=>true, 'message'=>'success', 'data'=>$data]);
        }else{
            return response()->json(['status'=>true, 'message'=>'No data found', 'data'=>[]]);
        }
    }

    public function save_highlight(Request $request){

        $validator = \Validator::make($request->all(), [
            'highlight' => 'required'
        ]);

        if ($validator->fails()) {
            $plucked = collect($validator->errors())->pluck(0);
            return response()->json(['status'=>false,'error'=>$plucked], 200);
        }

        $t = new Highlight();
        $t->user_id = Auth::id();
        $t->highlight = $request->highlight;
        if($t->save()){
            return response()->json(['status'=>true, 'message'=>'Successfully inserted in Highlights']);
        }else{
            return response()->json(['status'=>false, 'error'=>['Error']]);
        }

    }

    public function get_notes(){
        $data = Notes::where('user_id',Auth::id())->get();
        if($data){
            return response()->json(['status'=>true, 'message'=>'success', 'data'=>$data]);
        }else{
            return response()->json(['status'=>true, 'message'=>'No data found', 'data'=>[]]);
        }
    }

    public function save_notes(Request $request){
        $validator = \Validator::make($request->all(), [
            'notes' => 'required'
        ]);

        if ($validator->fails()) {
            $plucked = collect($validator->errors())->pluck(0);
            return response()->json(['status'=>false,'error'=>$plucked], 200);
        }

        $t = new Notes();
        $t->user_id = Auth::id();
        $t->notes = $request->notes;
        if($t->save()){
            return response()->json(['status'=>true, 'message'=>'Successfully inserted in Notes']);
        }else{
            return response()->json(['status'=>false, 'error'=>['Error']]);
        }
    }
}
