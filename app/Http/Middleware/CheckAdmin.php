<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('admin')->check()){
                return $next($request);
        }else{
            return redirect()->guest(route('admin.login'))->withErrors('Session Expired');
        }
        Auth::guard('admin')->logout();
        return redirect()->guest(route('admin.login'))->withErrors(' Invalid credentials ,  This section is for admin only');
    }
}
