<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSelection extends Model
{
    use SoftDeletes;
    protected $table = 'user_selection';
//    protected $fillable = ['cat_id', 'subcat_id', 'chapter_name', 'description', 'created_at', 'updated_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
