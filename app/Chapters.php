<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapters extends Model
{
    protected $table = "chapters";

    public function category(){
        return $this->hasOne("App\Category", 'id', 'cat_id');
    }

    public function subCategory(){
        return $this->hasOne("App\Subcat", 'id', 'subcat_id');
    }
}
