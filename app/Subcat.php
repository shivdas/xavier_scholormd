<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Subcat extends Model
{
    use SoftDeletes;
    protected $table = 'sub_categories';
    protected $fillable = ['cat_id', 'subcat_name','sub_title', 'created_at', 'updated_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
