<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model
{
    use SoftDeletes;
    protected $table = 'categories';
    protected $fillable = ['cat_name','sub_title', 'created_at', 'updated_at'];

    public function subcategory(){
        return $this->hasMany('App\Subcat','cat_id','id')->select(['id','cat_id','subcat_name','sub_title','slug']);
    }

}
