<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Medical extends Model
{
    use SoftDeletes;
    protected $table = 'medical_post';
    protected $fillable = ['cat_name','sub_title', 'created_at', 'updated_at'];

}
