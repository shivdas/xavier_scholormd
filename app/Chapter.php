<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Chapter extends Model
{
    use SoftDeletes;
    protected $table = 'chapters';
    protected $fillable = ['cat_id', 'subcat_id', 'chapter_name','sub_title', 'description', 'created_at', 'updated_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
