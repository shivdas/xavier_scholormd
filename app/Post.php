<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';
    protected $fillable = ['category_id', 'post_title', 'post_content', 'created_at', 'updated_at'];
}
