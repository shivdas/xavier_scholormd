<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Fact extends Model
{
    //
    protected $fillable = ['name', 'description', 'fact_date'];
    use SoftDeletes;
}
