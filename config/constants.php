<?php

return [

    'app_name' => 'ScholarMD',

    /* APPLICATION LINK */
    'app_download_link' => 'xxxxxxxx',

    /* PUSH NOTIFICATION CONFIG - ANDROID */
    'google_url'                    => "https://fcm.googleapis.com/fcm/send",
    'android_google_authorized_key' => "AIzaSyBkECEVADUrTyxD3fQU2rcFKpbgWxbcqaE",

    /* PUSH NOTIFICATION CONFIG - APPLE IOS */
    'passphrase' => '1234',

];
